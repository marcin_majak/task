#include "ProgramArguments.h"

namespace pa
{
	std::tuple<bool, cxxopts::ParseResult> parse(int argc, char* argv[])
	{
		try
		{
			cxxopts::Options options(argv[0], " - example command line options");
			options
				.positional_help("[optional args]")
				.show_positional_help();

			bool apple = false;

			options
				.allow_unrecognised_options()
				.add_options()
				("d,directory", "DirectoryPath", cxxopts::value<std::string>())
				("help", "Print help");

			cxxopts::ParseResult result = options.parse(argc, argv);

			const auto& arguments = result.arguments();

			const auto showHelpForInvalidArguments = arguments.size() != 1 || !result.count("directory");

			if (result.count("help") || showHelpForInvalidArguments)
			{
				std::cout << options.help({ "", "Group" }) << std::endl;
				return std::make_tuple(false, result);
			}

			return std::make_tuple(true, result);
		}
		catch (const cxxopts::OptionException& e)
		{
			std::cout << "error parsing options: " << e.what() << std::endl;
		}

		return std::make_tuple(true, cxxopts::ParseResult({}, {}, false, argc, argv));
	}
}