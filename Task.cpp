﻿#include <IImageReconstructionAlgorithm.h>
#include <StlContainerFactory.h>
#include <IOWrapper.h>
#include <IStatisticsCalculatorFactory.h>
#include <filesystem>
#include <tuple>
#include <ILogger.h>
#include "ProgramArguments.h"
namespace fs = std::experimental::filesystem;

/*
	Run this program as follows: Task.exe --directory="path with images"
*/


int main(int argc, char* argv[])
{
	auto result = pa::parse(argc, argv);
	std::shared_ptr<core::ILogger> logger = core::CreateConsoleLogger();

	const auto areParametersValid = std::get<0>(result);
	if (!areParametersValid)
	{
		logger->Log("Invalid arguments passed");
		return 1;
	}

	const auto parsedCommandLine = std::get<1>(result);

	auto directoryPath = parsedCommandLine["directory"].as<std::string>();
	logger->Log("Reading data from: " + directoryPath);

	std::vector<core::Image_8u_C3> images;

	auto io_wrapper = io::CreateIOWrapper();

	for (auto& p : fs::directory_iterator(directoryPath))
	{
		images.emplace_back(io_wrapper->Read(p.path().string()));
	}

	std::shared_ptr<core::IContainerFactory> containerFactory = core::CreateStlContainerFactory();
	std::shared_ptr<ip::ICopyFilterFactory> copyFilterFactory = ip::CreateCopyFilterFactory(containerFactory);
	std::shared_ptr<ip::ISmoothingFilterFactory> smoothingFilterFactory = ip::CreateSmoothingFactory(copyFilterFactory);
	std::shared_ptr<ip::IThresholdingFilterFactory> thresholdingFilterFactory = ip::CreateThresholdingFilterFactory();
	std::shared_ptr<ip::IConverterFactory> converterFactory = ip::CreateConverterFactory();
	std::shared_ptr<ip::IImageCalculatorFactory> imageCalculatorFactory = ip::CreateImageCalculatorFactory();
	std::shared_ptr<ip::IStatisticsCalculatorFactory> statisticsCalculatorFactory = ip::CreateStatisticsCalculatorFactory();

	try
	{
		auto reconstructionAlgorithm = alg::CreateReconstructionAlgorithm(core::CreateStlContainerFactory(),
			smoothingFilterFactory,
			thresholdingFilterFactory,
			converterFactory,
			imageCalculatorFactory,
			statisticsCalculatorFactory,
			logger);

		auto reconstructedData = reconstructionAlgorithm->Process(images);

		auto outputPath = R"(result.png)";
		io_wrapper->Write(std::get<0>(reconstructedData), outputPath);
		logger->Log(std::string("Result saved to: ") + outputPath);

		auto outputDepthMapPath = R"(resultDepthMap.png)";
		io_wrapper->Write(std::get<1>(reconstructedData), outputDepthMapPath);
		logger->Log(std::string("Depth map saved to: ") + outputDepthMapPath);
	}
	catch (const std::exception& ex)
	{
		logger->Log("Finished with error:");
		logger->Log(ex.what());

		return 1;
	}

	return 0;
}
