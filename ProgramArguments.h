#ifndef ProgramArguments_H
#define ProgramArguments_H

#include <cxxopts.hpp>
#include <tuple>

namespace pa
{
	std::tuple<bool, cxxopts::ParseResult> parse(int argc, char* argv[]);
}

#endif