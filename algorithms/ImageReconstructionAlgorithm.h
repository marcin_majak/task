#pragma once

#ifndef ImageReconstructionAlgorithm_H
#define ImageReconstructionAlgorithm_H

#include "IImageReconstructionAlgorithm.h"
#include <IContainerFactory.h>
#include <ISmoothingFilterFactory.h>
#include <ThresholdingFilterFactory.h>
#include <IConverterFactory.h>
#include <IImageCalculatorFactory.h>
#include <IStatisticsCalculatorFactory.h>

namespace alg
{
	class ImageReconstructionAlgorithm : public IImageReconstructionAlgorithm
	{
	public:
		ImageReconstructionAlgorithm(std::shared_ptr<core::IContainerFactory> containerFactory,
			std::shared_ptr<ip::ISmoothingFilterFactory> smoothingFactory,
			std::shared_ptr<ip::IThresholdingFilterFactory> thresholdingFactory,
			std::shared_ptr<ip::IConverterFactory> converterFactory,
			std::shared_ptr<ip::IImageCalculatorFactory> imageCalculatorFactory,
			std::shared_ptr<ip::IStatisticsCalculatorFactory> statisticsCalculatorFactory,
			std::shared_ptr<core::ILogger> logger);

		std::tuple<core::Image_8u_C3, core::Image_8u> Process(const std::vector<core::Image_8u_C3>& src) override;

	private:
		core::Image_32f GenerateDepthMap(const std::vector<core::Image_32f>& images) const;

		std::shared_ptr<core::IContainerFactory> _containerFactory;
		std::shared_ptr<ip::ISmoothingFilterFactory> _smoothingFactory;
		std::shared_ptr<ip::IThresholdingFilterFactory> _thresholdingFactory;
		std::shared_ptr<ip::IConverterFactory> _converterFactory;
		std::shared_ptr<ip::IImageCalculatorFactory> _imageCalculatorFactory;
		std::shared_ptr<ip::IStatisticsCalculatorFactory> _statisticsCalculatorFactory;
		std::shared_ptr<core::ILogger> _logger;
	};
}

#endif