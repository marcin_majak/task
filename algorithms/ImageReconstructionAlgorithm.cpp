#include "ImageReconstructionAlgorithm.h"
#include <limits>
#include <iostream>
#include "StatisticsCalculator.h"
#include <algorithm>

namespace alg
{
	ImageReconstructionAlgorithm::ImageReconstructionAlgorithm(
		std::shared_ptr<core::IContainerFactory> containerFactory,
		std::shared_ptr<ip::ISmoothingFilterFactory> smoothingFactory,
		std::shared_ptr<ip::IThresholdingFilterFactory> thresholdingFactory,
		std::shared_ptr<ip::IConverterFactory> converterFactory,
		std::shared_ptr<ip::IImageCalculatorFactory> imageCalculatorFactory,
		std::shared_ptr<ip::IStatisticsCalculatorFactory> statisticsCalculatorFactory,
		std::shared_ptr<core::ILogger> logger) :
			_containerFactory(std::move(containerFactory)),
			_smoothingFactory(std::move(smoothingFactory)),
			_thresholdingFactory(std::move(thresholdingFactory)),
			_converterFactory(std::move(converterFactory)),
			_imageCalculatorFactory(std::move(imageCalculatorFactory)),
			_statisticsCalculatorFactory(std::move(statisticsCalculatorFactory)),
			_logger(std::move(logger))
	{
		ASSERT_NOT_NULL(_containerFactory);
		ASSERT_NOT_NULL(_smoothingFactory);
		ASSERT_NOT_NULL(_thresholdingFactory);
		ASSERT_NOT_NULL(_converterFactory);
		ASSERT_NOT_NULL(_imageCalculatorFactory);
		ASSERT_NOT_NULL(_statisticsCalculatorFactory);
		ASSERT_NOT_NULL(_logger);
	}

	std::tuple<core::Image_8u_C3, core::Image_8u> ImageReconstructionAlgorithm::Process(
		const std::vector<core::Image_8u_C3>& src)
	{
		if (src.empty())
			THROW_ARGUMENT_EXCEPTION(src, "Images list cannot be empty");

		const auto imageSize = src.front().size_pixels();
		const int gaussWindowSize = 21;

		const auto converter = _converterFactory->CreateConverter();
		auto gaussFilter = _smoothingFactory->CreateGaussFilter(gaussWindowSize);
		const auto imageCalculator = _imageCalculatorFactory->CreateImageCalculator();
		const auto thresholdFilter = _thresholdingFactory->CreateLessThanThresholdFilter_32f();
		auto statisticsCalculator = _statisticsCalculatorFactory->CreateStatisticsCalculator();

		auto floatImage = _containerFactory->Create_32f().Image(imageSize);
		auto gaussImage = _containerFactory->Create_32f().Image(imageSize);
		auto accumulatorImage = _containerFactory->Create_32f().Image(imageSize);
		accumulatorImage.fill(std::numeric_limits<float>::lowest());

		auto indexerImage = _containerFactory->Create_16u().Image(imageSize);
		auto maskImage = _containerFactory->Create_bool().Image(imageSize);

		std::vector<core::Image_32f> differenceImages;
		differenceImages.reserve(src.size());

		int imageIndex = 0;
		for (const auto& srcImage : src)
		{
			_logger->Log("Starting processing image-" + std::to_string(imageIndex + 1));
			converter->Convert(srcImage, floatImage);
			gaussFilter->Process(floatImage, gaussImage);
			imageCalculator->Sub_I(floatImage, gaussImage);
			imageCalculator->Abs_I(floatImage);

			auto differenceImage = _containerFactory->Create_32f().Image(imageSize);
			differenceImage.copy_from(floatImage);
			differenceImages.emplace_back(std::move(differenceImage));

			thresholdFilter->Process(accumulatorImage, floatImage, maskImage);
			imageCalculator->CopyWithMask(floatImage, maskImage, accumulatorImage);
			imageCalculator->SetWithMask(indexerImage, maskImage, imageIndex);
			
			_logger->Log("Finished processing image-" + std::to_string(imageIndex + 1));

			++imageIndex;
		}

		auto reconstructedImage = _containerFactory->Create_8u().Image_C3(imageSize);
		auto depthMapImage = _containerFactory->Create_8u().Image(imageSize);

		_logger->Log("Final image reconstruction begins");
		for (auto y=0; y<int(reconstructedImage.height()); ++y)
		{
			auto indexerImagePtr = indexerImage.ptr(0, y);
			auto reconstructedImagePtr = reconstructedImage.ptr(0, y);

			for (auto x=0; x<int(reconstructedImage.width()); ++x)
			{
				const auto srcImageIndex = *indexerImagePtr;
				const auto srcPtr = src[srcImageIndex].ptr(3 * x, y);

				memcpy(reconstructedImagePtr, srcPtr, 3);

				++indexerImagePtr;
				reconstructedImagePtr += 3;
			}
		}

		auto result = GenerateDepthMap(differenceImages);
		auto minMaxResult = statisticsCalculator->GetMinMax(result);
		converter->Convert(result, std::get<0>(minMaxResult), std::get<1>(minMaxResult), depthMapImage);

		_logger->Log("Final image reconstruction ends");

		return std::make_tuple(std::move(reconstructedImage), std::move(depthMapImage));
	}

	core::Image_32f ImageReconstructionAlgorithm::GenerateDepthMap(const std::vector<core::Image_32f>& images) const
	{
		const core::Size imageSize = images.front().size_pixels();
		auto result_32f = _containerFactory->Create_32f().Image(imageSize);	
		for (int j = 0; j < int(imageSize.height); j++)
		{

			auto result_32fPtr = result_32f.ptr(0, j);

			for (int i = 0; i < int(imageSize.width); ++i, ++result_32fPtr)
			{
				double weights = 0;
				double sum = 0;

				for (int k = 0; k < int(images.size()); ++k)
				{
					auto& scan = images[k];
					const auto scanPtr = scan.ptr(i, j);
					const double weight = (int(images.size()) - k) * std::pow((*scanPtr/255.0), 2);

					sum += *scanPtr * weight;
					weights += weight;
				}

				*result_32fPtr = static_cast<float>(std::round(sum / (weights + std::numeric_limits<double>::epsilon())));
			}
		}

		return result_32f;
	}
}
