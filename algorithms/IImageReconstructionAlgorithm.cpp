#include "IImageReconstructionAlgorithm.h"
#include "ImageReconstructionAlgorithm.h"

namespace alg
{
	IImageReconstructionAlgorithm::~IImageReconstructionAlgorithm() = default;

	std::unique_ptr<IImageReconstructionAlgorithm> CreateReconstructionAlgorithm(
		std::shared_ptr<core::IContainerFactory> containerFactory,
		std::shared_ptr<ip::ISmoothingFilterFactory> smoothingFactory,
		std::shared_ptr<ip::IThresholdingFilterFactory> thresholdingFactory,
		std::shared_ptr<ip::IConverterFactory> converterFactory,
		std::shared_ptr<ip::IImageCalculatorFactory> imageCalculatorFactory,
		std::shared_ptr<ip::IStatisticsCalculatorFactory> statisticsCalculatorFactory,
		std::shared_ptr<core::ILogger> logger)
	{
		return std::make_unique<ImageReconstructionAlgorithm>(std::move(containerFactory),
			std::move(smoothingFactory),
			std::move(thresholdingFactory),
			std::move(converterFactory),
			std::move(imageCalculatorFactory),
			std::move(statisticsCalculatorFactory),
			std::move(logger));
	}
}
