#pragma once
#ifndef Algorithms_ExportDefines_H
#define Algorithms_ExportDefines_H

#ifdef ALG_EXPORTING
#define ALG_API __declspec(dllexport)
#else
#define ALG_API __declspec(dllimport)
#endif

#endif