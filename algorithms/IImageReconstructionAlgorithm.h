#pragma once

#ifndef IImageReconstructionAlgorithm_H
#define IImageReconstructionAlgorithm_H

#include <tuple>
#include <vector>
#include <memory>
#include <Image.h>
#include <IImageCalculatorFactory.h>
#include <IConverterFactory.h>
#include <ThresholdingFilterFactory.h>
#include <ISmoothingFilterFactory.h>
#include <IContainerFactory.h>
#include <IStatisticsCalculatorFactory.h>
#include <ILogger.h>
#include "ExportDefines.h"

namespace alg
{
	struct ALG_API IImageReconstructionAlgorithm
	{
	public:
		virtual ~IImageReconstructionAlgorithm();

		virtual std::tuple<core::Image_8u_C3, core::Image_8u> Process(const std::vector<core::Image_8u_C3>& src) = 0;
	};

	std::unique_ptr<IImageReconstructionAlgorithm> ALG_API CreateReconstructionAlgorithm(std::shared_ptr<core::IContainerFactory> containerFactory,
		std::shared_ptr<ip::ISmoothingFilterFactory> smoothingFactory,
		std::shared_ptr<ip::IThresholdingFilterFactory> thresholdingFactory,
		std::shared_ptr<ip::IConverterFactory> converterFactory,
		std::shared_ptr<ip::IImageCalculatorFactory> imageCalculatorFactory,
		std::shared_ptr<ip::IStatisticsCalculatorFactory> statisticsCalculatorFactory,
		std::shared_ptr<core::ILogger> logger);
	
}
#endif