#pragma once

#ifndef BaseDataFormatProvider_H
#define BaseDataFormatProvider_H

#include "ImageData.h"

namespace core
{
	namespace fmt
	{
		/// <summary>
		/// Base image format
		/// </summary>
		template<typename _Ty, size_t _Channels>
		struct BaseDataFormatProvider
		{
		public:

			using pointer = typename ImageData<_Ty>::pointer;
			using const_pointer = const pointer;
			using channel_count_type = size_t;

			/// <summary>
			/// Gets number of channels for format
			/// </summary>
			/// <returns></returns>
			static channel_count_type channels() { return _Channels; }

			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="data">image data</param>
			BaseDataFormatProvider(
				const ImageData<_Ty>& data) :
				_data(data)
			{}

		protected:

			/// <summary>
			/// Image data view
			/// </summary>
			ImageData<_Ty> _data;
		};
	}
}

#endif