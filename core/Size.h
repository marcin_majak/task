#pragma once

#ifndef Size_H
#define Size_H

#include "EqualityHelpers.h"
#include "ExportDefines.h"

namespace core
{
	template<typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>::type>
	class Size_
	{
	public:

		/// <summary> The width. </summary>
		T width;

		/// <summary> The height. </summary>
		T height;

		/// <summary> Default constructor. </summary>
		Size_() : width(0), height(0)
		{

		}

		Size_(T _width, T _height) : width(_width), height(_height)
		{

		}

		template<class _AreaType = T>
		_AreaType area() const
		{
			return static_cast<_AreaType>(width) * static_cast<_AreaType>(height);
		}


		bool operator==(const Size_& right) const
		{
			return this->width == right.width && this->height == right.height;
		}

		bool operator!=(const Size_& right) const
		{
			return !(*this == right);
		}

		Size_(const Size_& other) = default;

		Size_(Size_ && other) noexcept = default;

		Size_& operator=(const Size_& other) = default;

		Size_ & operator=(Size_ && other) noexcept = default;
	};

	template<>
	inline bool Size_<float>::operator==(const Size_<float>& right) const
	{
		return equal_epsilon(width, right.width) && equal_epsilon(height, right.height);
	}

	template<>
	inline bool Size_<float>::operator!=(const Size_<float>& right) const
	{
		return !(this->operator==(right));
	}

	template<>
	inline bool Size_<double>::operator==(const Size_<double>& right) const
	{
		return equal_epsilon(width, right.width) && equal_epsilon(height, right.height);
	}

	template<>
	inline bool Size_<double>::operator!=(const Size_<double>& right) const
	{
		return !(this->operator==(right));
	}

	using Size2i = Size_<int>;
	using Size2f = Size_<float>;
	using Size2d = Size_<double>;
	using Size = Size_<size_t>;

	template class CORE_API Size_<int>;
	template class CORE_API Size_<float>;
	template class CORE_API Size_<double>;
	template class CORE_API Size_<size_t>;

}

#endif