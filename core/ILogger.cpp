#include "ILogger.h"
#include "ConsoleLogger.h"

namespace core
{
	ILogger::~ILogger() = default;
	
	std::unique_ptr<ILogger> CreateConsoleLogger()
	{
		return std::make_unique<ConsoleLogger>();
	}
}