#pragma once
#ifndef ImageMemoryManager_H
#define ImageMemoryManager_H

#include <memory>
#include "IImageAllocator.h"

namespace core
{
	template<typename _Ty>
	class ImageMemoryManager
	{
		/// <summary>
		/// Structure describing allocated memory
		/// </summary>
		_Ty* _dataPtr;

		/// <summary>
		/// Allocator
		/// </summary>
		std::shared_ptr<IImageAllocator<_Ty>> _allocator;

	public:
		ImageMemoryManager(
			std::shared_ptr<IImageAllocator<_Ty>> allocator) :
			_dataPtr(nullptr),
			_allocator(std::move(allocator))
		{
			ASSERT_NOT_NULL(_allocator);
			// This function accepts null ptr allocator, and we assume that the allocator is checked before is used.
		}

		virtual ~ImageMemoryManager()
		{
			dealloc();
		}

		void dealloc()
		{
			if (_dataPtr != nullptr)
			{
				_allocator->dealloc(_dataPtr);
				_dataPtr = nullptr;
			}
		}

		ImageData<_Ty> alloc(
			const Size& sizePixels,
			size_t channels)
		{
			auto handle = _allocator->alloc(
				sizePixels,
				channels);
			_dataPtr = handle.ptr;
			return handle;
		}

		void copy(
			const ImageData<_Ty>& src,
			ImageData<_Ty>& dst) const
		{
			// Skip copying when both data pointers are empty
			if (data_empty(src) && data_empty(dst))
				return;

			if (!have_data_same_size(src, dst))
				THROW_ARGUMENT_EXCEPTION(src, "Size of src and dst cannot differ.");

			_allocator->copy(src, dst);
		}

		void fill(
			ImageData<_Ty>& data,
			_Ty value) const
		{
			// Skip the fill operation when the image is empty
			if (data.ptr == nullptr)
				return;

			_allocator->fill(data, value);
		}

		std::shared_ptr<IImageAllocator<_Ty>> allocator() const
		{
			return _allocator;
		}
	};

}

#endif