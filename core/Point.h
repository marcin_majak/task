#pragma once
#ifndef Point_H
#define Point_H

#include <type_traits>
#include "EqualityHelpers.h"

namespace core
{
	template<typename _Tp, typename = typename std::enable_if<std::is_arithmetic<_Tp>::value>::type>
	class Point_
	{

	public:

		Point_() : x{ 0 }, y{ 0 } {  }

		Point_(_Tp _x, _Tp _y) : x(_x), y(_y)
		{
		}

		Point_(const Point_& other) = default;

		Point_(Point_ && other) = default;

		Point_& operator=(const Point_& other) = default;

		Point_ & operator=(Point_ && other) = default;

		bool operator==(const Point_& right) const
		{
			return this->x == right.x && this->y == right.y;
		}

		bool operator!=(const Point_& right) const
		{
			return !(*this == right);
		}

		Point_ operator - (const Point_& right) const
		{
			return{ x - right.x, y - right.y };
		}

		Point_ operator + (const Point_& right) const
		{
			return{ x + right.x, y + right.y };
		}

		Point_ operator * (_Tp scalar) const
		{
			return{ x * scalar, y * scalar };
		}


		/// <summary> The x coordinate. </summary>
		_Tp x;

		/// <summary> The y coordinate. </summary>
		_Tp y;
	};


	template<>
	inline bool Point_<float>::operator==(const Point_<float>& right) const
	{
		return equal_epsilon(x, right.x) &&
			equal_epsilon(y, right.y);
	}

	template<>
	inline bool Point_<float>::operator!=(const Point_<float>& right) const
	{
		return !(*this == right);
	}

	template<>
	inline bool Point_<double>::operator==(const Point_<double>& right) const
	{
		return equal_epsilon(x, right.x) &&
			equal_epsilon(y, right.y);
	}

	template<>
	inline bool Point_<double>::operator!=(const Point_<double>& right) const
	{
		return !(*this == right);
	}

	using Point2i = Point_<int>;
	using Point2ui = Point_<unsigned int>;
	using Point2ull = Point_<size_t>;
	using Point2f = Point_<float>;
	using Point2d = Point_<double>;
	using Point = Point2i;
}

#endif