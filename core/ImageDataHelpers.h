#pragma once

#ifndef ImageDataHelpers_H
#define ImageDataHelpers_H
#include "Exceptions.h"
#include "ImageData.h"
#include "Size.h"
#include "ImageMemoryHelpers.h"

namespace core
{
	template<typename T>
	inline bool have_data_same_size(
		const ImageData<T>& a,
		const ImageData<T>& b)
	{
		return (a.width == b.width) &&
			(a.height == b.height) &&
			(a.channels == b.channels);
	}

	template<typename T>
	inline bool data_empty(
		const ImageData<T>& data)
	{
		return
			data.ptr == nullptr ||
			data.width == 0 ||
			data.height == 0 ||
			data.step == 0 ||
			data.channels == 0;
	}

	template<typename T>
	inline ImageData<T> alloc_data(
		const Size& sizePixels,
		size_t channels)
	{
		size_t step = sizeof(T) * sizePixels.width * channels;

		return ImageData<T>{
			alloc_array<T>(sizePixels.width, sizePixels.height, channels),
				sizePixels.width,
				sizePixels.height,
				channels,
				step};
	}

	template<typename T>
	inline void fill_data(
		ImageData<T>& data,
		T value)
	{
		if (data.ptr == nullptr)
			return;

		auto numberOfElements = data.width * data.channels;
		auto ptr = data.ptr;
		for (auto y = 0; y < data.height; ++y)
		{
			std::fill_n(
				ptr,
				numberOfElements,
				value);
			ptr = add_bytes_to_ptr(ptr, data.step);
		}
	}

	template<typename T>
	void copy_data_2d(
		const ImageData<T>& src,
		ImageData<T>& dst)
	{
		// Skip copying when both data pointers are empty
		if (data_empty(src) && data_empty(dst))
			return;

		if (!have_data_same_size(src, dst))
			THROW_ARGUMENT_EXCEPTION(src, "Size of src and dst cannot differ.");

		auto lineByteWidth = src.width * src.channels * sizeof(T);
		auto dstPtr = dst.ptr;
		auto srcPtr = src.ptr;

		for (int y = 0; y < int(src.height); y++)
		{
			memcpy(dstPtr, srcPtr, lineByteWidth);
			srcPtr = add_bytes_to_ptr(srcPtr, src.step);
			dstPtr = add_bytes_to_ptr(dstPtr, dst.step);
		}
	}
}

#endif
