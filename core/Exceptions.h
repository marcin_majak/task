#pragma once

#ifndef Exceptions_H
#define Exceptions_H

#include <string>
#include <sstream>
#include "CoreHelpers.h"

namespace core
{
#define TYPE_NAME(type) std::string(#type)

	inline std::string create_message(
		const char* file,
		int line,
		const std::string& type,
		const std::string& message)
	{
		std::stringstream ss;
		ss << file << " (" << line << ") [" << type << "]: " << message;
		return ss.str();
	}

	inline std::string create_message(
		const char* file,
		int line,
		const std::string& type,
		const std::string& message,
		const char* innerException)
	{
		std::stringstream ss;
		ss << file << " (" << line << ") [" << type << "]: " << message << std::endl;
		ss << "Inner exception:" << std::endl << innerException;
		return ss.str();
	}

	inline std::string create_message(
		const std::string& type,
		const std::string& message)
	{
		std::stringstream ss;
		ss << "[" << type << "]: " << message;
		return ss.str();
	}

	inline std::string create_message(
		const std::string& type,
		const std::string& message,
		const char* innerException)
	{
		std::stringstream ss;
		ss << "[" << type << "]: " << message << std::endl;
		ss << "Inner exception:" << std::endl << innerException;
		return ss.str();
	}


	class Exception : public std::runtime_error
	{
	public:
		Exception(
			const std::string& message) :
			std::runtime_error(create_message(TYPE_NAME(Exception), message))
		{}

		Exception(
			const std::exception& inner,
			const std::string& message) :
			std::runtime_error(create_message(TYPE_NAME(Exception), message, inner.what()))
		{}

		Exception(
			const char* file,
			int line,
			const std::string& message) :
			std::runtime_error(create_message(file, line, TYPE_NAME(Exception), message))
		{}

		Exception(
			const std::exception& inner,
			const char* file,
			int line,
			const std::string& message) :
			std::runtime_error(create_message(file, line, TYPE_NAME(Exception), message, inner.what()))
		{}

		Exception(
			const std::string& type,
			const std::string& message) :
			std::runtime_error(create_message(type, message))
		{}

		Exception(
			const std::exception& inner,
			const std::string& type,
			const std::string& message) :
			std::runtime_error(create_message(type, message, inner.what()))
		{}

		Exception(
			const char* file,
			int line,
			const std::string& type,
			const std::string& message) :
			std::runtime_error(create_message(file, line, type, message))
		{}

		Exception(
			const std::exception& inner,
			const char* file,
			int line,
			const std::string& type,
			const std::string& message) :
			std::runtime_error(create_message(file, line, type, message, inner.what()))
		{}

		virtual ~Exception() = default;
	};

	template<class Deriving>
	class GenericException : public Exception
	{
		inline std::string type() const
		{
			return core::get_class_name<Deriving>();
		}

	public:

		GenericException(
			const std::string& message) :
			Exception(type(), message)
		{}

		GenericException(
			const std::exception& inner,
			const std::string& message) :
			Exception(inner, type(), message)
		{}

		GenericException(
			const char* file,
			int line,
			const std::string& message) :
			Exception(file, line, type(), message)
		{}

		GenericException(
			const std::exception& inner,
			const char* file,
			int line,
			const std::string& message) :
			Exception(inner, file, line, type(), message)
		{}
	};

	template<class Deriving>
	class GenericArgumentException : public Exception
	{
		inline std::string type(const std::string& argument) const
		{
			std::stringstream ss;
			ss << core::get_class_name<Deriving>() << " - " << argument;
			return ss.str();
		}

	public:

		GenericArgumentException(
			const std::string& argumentName,
			const std::string& message) :
			Exception(type(argumentName), message),
			_argumentName(argumentName)
		{}

		GenericArgumentException(
			const std::exception& inner,
			const std::string& argumentName,
			const std::string& message) :
			Exception(inner, type(argumentName), message),
			_argumentName(argumentName)
		{}

		GenericArgumentException(
			const char* file,
			int line,
			const std::string& argumentName,
			const std::string& message) :
			Exception(file, line, type(argumentName), message),
			_argumentName(argumentName)
		{}

		GenericArgumentException(
			const std::exception& inner,
			const char* file,
			int line,
			const std::string& argumentName,
			const std::string& message) :
			Exception(inner, file, line, type(argumentName), message),
			_argumentName(argumentName)
		{}


		const std::string& argument() const
		{
			return _argumentName;
		}

	private:
		std::string _argumentName;
	};

	class ArgumentException : public GenericArgumentException<ArgumentException>
	{
	public:

		ArgumentException(
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(argumentName, message)
		{}

		ArgumentException(
			const std::exception& inner,
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(inner, argumentName, message)
		{}

		ArgumentException(
			const char* file,
			int line,
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(file, line, argumentName, message)
		{}

		ArgumentException(
			const std::exception& inner,
			const char* file,
			int line,
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(inner, file, line, argumentName, message)
		{}
	};

	class ArgumentNullException : public GenericArgumentException<ArgumentNullException>
	{
	public:

		ArgumentNullException(
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(argumentName, message)
		{}

		ArgumentNullException(
			const std::exception& inner,
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(inner, argumentName, message)
		{}

		ArgumentNullException(
			const char* file,
			int line,
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(file, line, argumentName, message)
		{}

		ArgumentNullException(
			const std::exception& inner,
			const char* file,
			int line,
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(inner, file, line, argumentName, message)
		{}
	};

	class ArgumentOutOfRangeException : public GenericArgumentException<ArgumentOutOfRangeException>
	{
	public:

		ArgumentOutOfRangeException(
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(argumentName, message)
		{}

		ArgumentOutOfRangeException(
			const std::exception& inner,
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(inner, argumentName, message)
		{}

		ArgumentOutOfRangeException(
			const char* file,
			int line,
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(file, line, argumentName, message)
		{}

		ArgumentOutOfRangeException(
			const std::exception& inner,
			const char* file,
			int line,
			const std::string& argumentName,
			const std::string& message) :
			GenericArgumentException(inner, file, line, argumentName, message)
		{}
	};

#define CHECK_ARGUMENT(argument) if(argument == nullptr) throw core::ArgumentNullException( __FILE__, __LINE__, #argument, std::string(#argument) + " cannot be null")
#define THROW_EXCEPTION(message) throw core::Exception(__FILE__, __LINE__, message)
#define THROW_ARGUMENT_OUT_OF_RANGE(argument) throw core::ArgumentOutOfRangeException(__FILE__, __LINE__, #argument, std::string(#argument) + " cannot exceed bounds")
#define INVALID_OPERATION_EXCEPTION(message) THROW_EXCEPTION(message)
#define THROW_IPP_EXCEPTION(errorCode) throw core::IppException(__FILE__, __LINE__, errorCode)
#define ASSERT_IPP_STATUS(status) if (status != ippStsNoErr) THROW_IPP_EXCEPTION(status)
#define RETHROW_EXCEPTION(inner, message) throw core::Exception(inner, __FILE__, __LINE__, message)
#define RETHROW_DEFAULT(inner) throw core::Exception(inner, __FILE__, __LINE__, __FUNCTION__ + std::string(" failed"))
#define THROW_ARGUMENT_EXCEPTION(argument, message) throw core::ArgumentException(__FILE__, __LINE__, #argument, message)
#define ASSERT_NOT_NULL(value) CHECK_ARGUMENT(value)
#define ASSERT_NOT_0(argument) if( (argument) == 0) THROW_ARGUMENT_EXCEPTION(argument, "value cannot be 0") 
#define ASSERT_TRUE_OR_EXCEPTION(argument, message) if( !static_cast<bool>(argument) ) THROW_ARGUMENT_EXCEPTION((argument), message) 
#define THROW_NOT_IMPLEMENTED_EXCEPTION() THROW_EXCEPTION(std::string(__FUNCTION__) + " is not implemented")

}

#endif