#pragma once
#ifndef Core_ExportDefines_H
#define Core_ExportDefines_H

#ifdef CORE_EXPORTING
#define CORE_API __declspec(dllexport)
#else
#define CORE_API __declspec(dllimport)
#endif

#endif