#pragma once

#ifndef ImageIterator_H
#define ImageIterator_H

#include <stdexcept>
#include <cstdint>

namespace core
{
	template <typename _Ty>
	class image_iterator
	{
	public:
		typedef image_iterator _Unchecked_type;

		using iterator_category = std::forward_iterator_tag;
		using value_type = _Ty;
		using reference = _Ty &;
		using dataPointer = _Ty *;
		using pointer = uint8_t *;
		using difference_type = std::ptrdiff_t;
		using size_type = std::size_t;
		using channel_stride_type = size_t;

		using non_const_type = typename std::remove_const<_Ty>::type;
		using non_const_type_ptr = non_const_type *;

		image_iterator(
			dataPointer ptr,
			size_type width,
			difference_type step,
			channel_stride_type channelStride = 1) :
			image_iterator(ptr, 0, 0, width, step, channelStride)
		{
		}

		image_iterator(
			dataPointer ptr,
			std::size_t x,
			std::size_t y,
			size_type width,
			difference_type step,
			channel_stride_type channelStride = 1)
			: _ptr(reinterpret_cast<pointer>(const_cast<non_const_type_ptr>(ptr)))
			, _x(x)
			, _y(y)
			, _width(width)
			, _padding(step - width * sizeof(value_type))
			, _channelStride(channelStride)
		{
			if (step < static_cast<difference_type>(width * sizeof(value_type)))
				throw std::invalid_argument("Stride must not be less than width.");
		}

		size_type x() const
		{
			return _x;
		}

		size_type y() const
		{
			return _y;
		}

		image_iterator& operator ++()
		{
			_ptr += _channelStride * sizeof(value_type);
			_x += _channelStride;

			if (_x == _width)
			{
				_x = 0;
				++_y;
				_ptr += _padding;
			}

			return *this;
		}

		image_iterator operator ++(int)
		{
			image_iterator temp(*this);
			++(*this);
			return temp;
		}


		reference operator *() const
		{
			return *reinterpret_cast<dataPointer>(_ptr);
		}

		pointer operator ->() const
		{
			return reinterpret_cast<dataPointer>(_ptr);
		}

		bool operator ==(const image_iterator& other) const
		{
			return _ptr == other._ptr;
		}

		bool operator != (const image_iterator& other) const
		{
			return !(*this == other);
		}

		size_type width() const
		{
			return _width;
		}

		size_type padding() const
		{
			return _padding;
		}

	protected:

		/// <summary>
		/// Resource handle as byte pointer
		/// </summary>
		pointer _ptr;

		/// <summary>
		/// X location of pointed data
		/// </summary>
		size_type _x;

		/// <summary>
		/// Y location of pointed data
		/// </summary>
		size_type _y;

		/// <summary>
		/// Width of data
		/// </summary>
		size_type _width;

		/// <summary>
		/// Padding of data
		/// </summary>
		difference_type _padding;

		/// <summary>
		/// How much increment at each step
		/// </summary>
		channel_stride_type _channelStride;
	};
}

#endif