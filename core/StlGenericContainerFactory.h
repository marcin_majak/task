#pragma once

#ifndef StlContainerFactory_H
#define StlContainerFactory_H

#include "IContainerFactory.h"
#include "Image.h"
#include "StlImageAllocator.h"

namespace core
{
	template<class T>
	class StlGenericContainerFactory : public GenericContainerFactory<T>
	{
		std::shared_ptr<IImageAllocator<T>> _imageAllocator;
	public:
		StlGenericContainerFactory() :
			_imageAllocator(std::make_shared<StlImageAllocator<T>>())
		{
		}

		core::Image<T> Image(size_t width, size_t height) const override
		{
			return core::Image<T>(Size(width, height), _imageAllocator);
		}

		core::Image<T, fmt::C3Format<T>> Image_C3(size_t width, size_t height) const override
		{
			return core::Image<T, fmt::C3Format<T>>(Size(width, height), _imageAllocator);
		}

		core::Image<T, fmt::C3Format<T>> Image_C3(const core::Size& size) const override
		{
			return core::Image<T, fmt::C3Format<T>>(size, _imageAllocator);
		}

		core::Image<T> Image(std::initializer_list<std::initializer_list<T>> initializer) const override
		{
			return core::Image<T>(_imageAllocator, initializer);
		}

		core::Image<T> Image(const core::Size& size) const override
		{
			return core::Image<T>(size, _imageAllocator);
		}
	};

	class StlContainerFactory : public IContainerFactory
	{
		StlGenericContainerFactory<bool> factoryFor_bool;
		StlGenericContainerFactory<uint8_t> factoryFor_8u;
		StlGenericContainerFactory<int16_t> factoryFor_16s;
		StlGenericContainerFactory<uint16_t> factoryFor_16u;
		StlGenericContainerFactory<int32_t> factoryFor_32s;
		StlGenericContainerFactory<float> factoryFor_32f;
	public:

		const IGenericContainerFactory<bool>& Create_bool() const override;

		const IGenericContainerFactory<uint8_t>& Create_8u() const override;

		const IGenericContainerFactory<uint16_t>& Create_16u() const override;

		const IGenericContainerFactory<int16_t>& Create_16s() const override;

		const IGenericContainerFactory<int32_t>& Create_32s() const override;

		const IGenericContainerFactory<float>& Create_32f() const override;

		~StlContainerFactory() override;
	};

	std::unique_ptr<IContainerFactory> CreateStlContainerFactory();
}
#endif