#pragma once

#ifndef Image_H
#define Image_H

#include <memory>
#include <cstdint>
#include <sstream>

#include "Size.h"
#include "Rect.h"
#include "ImageData.h"
#include "Exceptions.h"
#include "C3Format.h"
#include "IImageAllocator.h"
#include "GrayScaleFormat.h"
#include "ImageMemoryManager.h"
#include "ImageMemoryHelpers.h"
#include "StlImageAllocator.h"
#include "ImageIterator.h"
#include "ImageIteratorHelpers.h"


namespace core
{
	/// <summary>
/// Binds memory to certain image
/// </summary>
	template<typename _Ty, class _formatProvider = fmt::GrayScaleFormat<_Ty>>
	class Image
	{
	public:
		using coordinates_type = size_t;
		using size_type = coordinates_type;
		using size2d_type = Size_<coordinates_type>;
		using rect_type = Rect_<coordinates_type>;
		using point_type = Point_<coordinates_type>;
		using iterator = image_iterator<_Ty>;
		using const_iterator = image_iterator<const _Ty>;
		using pointer = _Ty *;
		using data_type = _Ty;
		using const_pointer = const _Ty*;
		using allocator_pointer = std::shared_ptr<IImageAllocator<_Ty>>;
		using ConstFormat = typename _formatProvider::ConstFormat;

		/// <summary>
		/// Gets width of an image in elements
		/// </summary>
		/// <returns>width</returns>
		coordinates_type width_elements() const
		{
			return _localData.width * _formatProvider::channels();
		}

		/// <summary>
		/// Gets width of an image in pixels
		/// </summary>
		/// <returns>width</returns>
		coordinates_type width_pixels() const
		{
			return _localData.width;
		}

		/// <summary>
		/// Gets width of an image in pixels
		/// </summary>
		/// <returns>width</returns>
		coordinates_type width() const
		{
			return _localData.width;
		}

		/// <summary>
		/// Gets height of an image
		/// </summary>
		/// <returns></returns>
		coordinates_type height() const
		{
			return _localData.height;
		}

		/// <summary>
		/// Gets size of an image in pixels
		/// </summary>
		/// <returns></returns>
		Size size_pixels() const
		{
			return{ width_pixels(), height() };
		}

		/// <summary>
		/// Gets size of an image in pixels
		/// </summary>
		/// <returns></returns>
		template<typename T>
		Size_<T> size_pixels_t() const
		{
			return{ static_cast<T>(width_pixels()), static_cast<T>(height()) };
		}

		/// <summary>
		/// Gets size
		/// </summary>
		/// <returns></returns>
		Size size_elements() const
		{
			return{ this->width_elements(), this->height() };
		}

		/// <summary>
		/// Gets format provider
		/// </summary>
		/// <returns>Format provider</returns>
		_formatProvider format()
		{
			return _formatProvider(_localData);
		}

		/// <summary>
		/// Gets format provider
		/// </summary>
		/// <returns>Format provider</returns>
		ConstFormat format() const
		{
			return ConstFormat(ImageData<const _Ty>
			{
				_localData.ptr,
					_localData.width,
					_localData.height,
					_localData.channels,
					_localData.step
			});
		}

		/// <summary>
		/// Gets number of image channels
		/// </summary>
		/// <returns>Number of channels</returns>
		typename _formatProvider::channel_count_type channels() const
		{
			return _formatProvider::channels();
		}

		/// <summary>
		/// Gets step of the image
		/// </summary>
		/// <returns>Step as number of bytes</returns>
		size_type step() const
		{
			return _localData.step;
		}

		/// <summary>
		/// Check if size of image is 0
		/// </summary>
		/// <returns>True if size is 0</returns>
		bool empty() const
		{
			return data_empty(_localData);
		}

		pointer ptr()
		{
			return _localData.ptr;
		}

		const_pointer ptr() const
		{
			return _localData.ptr;
		}

		/// <summary>
		/// Gets pointer to the element in the array
		/// </summary>
		/// <param name="x">position in x direction</param>
		/// <param name="y">position in y direction</param>
		/// <returns>pointer to the specified element</returns>
		pointer ptr(
			coordinates_type x,
			coordinates_type y)
		{
			if (_localData.ptr == nullptr)
				return nullptr;

#if DEBUG || SAFE_INDEXING
			ASSERT_NOT_NULL(_localData.ptr);

			if (x >= width_elements())
				THROW_ARGUMENT_OUT_OF_RANGE(x);

			if (y >= _localData.height)
				THROW_ARGUMENT_OUT_OF_RANGE(y);
#endif			
			return getPtr(x, y);
		}

		/// <summary>
		/// Gets pointer to the element in the array
		/// </summary>
		/// <param name="x">position in x direction</param>
		/// <param name="y">position in y direction</param>
		/// <returns>pointer to the specified element</returns>
		const_pointer ptr(coordinates_type x, coordinates_type y) const
		{
			if (_localData.ptr == nullptr)
				return nullptr;

#if DEBUG || SAFE_INDEXING
			ASSERT_NOT_NULL(_localData.ptr);

			if (x >= width_elements())
				THROW_ARGUMENT_OUT_OF_RANGE(x);

			if (y >= _localData.height)
				THROW_ARGUMENT_OUT_OF_RANGE(y);
#endif			
			return getPtr(x, y);
		}

		/// <summary>
		/// Gets iterator to the first element
		/// </summary>
		/// <returns>Iterator to the first element</returns>
		iterator begin()
		{
			return core::create_begin_iterator<_Ty>(
				_localData.ptr,
				_localData);
			}

		/// <summary>
		/// Gets iterator to the first element
		/// </summary>
		/// <returns>Iterator to the first element</returns>
		const_iterator begin() const
		{
			return core::create_begin_iterator<const _Ty>(
				_localData.ptr,
				_localData);
		}

		/// <summary>
		/// Gets iterator to the certain element
		/// </summary>
		/// <returns>Iterator to the certain element</returns>
		iterator begin(size_t x, size_t y)
		{
			return core::create_iterator<_Ty>(
				_localData.ptr,
				_localData,
				x,
				y);
		}

		/// <summary>
		/// Gets iterator to the certain element
		/// </summary>
		/// <returns>Iterator to the certain element</returns>
		const_iterator begin(size_t x, size_t y) const
		{
			return core::create_iterator<const _Ty>(
				_localData.ptr,
				_localData,
				x,
				y);
		}

		/// <summary>
		/// Gets iterator to the end element
		/// </summary>
		/// <returns>Iterator to the end element</returns>
		iterator end()
		{
			return create_end_iterator<_Ty>(
				_localData.ptr,
				_localData);
		}

		/// <summary>
		/// Gets iterator to the end element
		/// </summary>
		/// <returns>Iterator to the end element</returns>
		const_iterator end() const
		{
			return create_end_iterator<const _Ty>(
				_localData.ptr,
				_localData);
		}

		/// <summary>
		/// Gets pointer to certain row
		/// </summary>
		/// <param name="y">Position in y in data array coordinate system</param>
		/// <returns>Pointer to row</returns>
		pointer operator[](coordinates_type y)
		{
			if (y >= _localData.height)
				THROW_ARGUMENT_OUT_OF_RANGE(y);

			return getPtr(0, y);
		}

		/// <summary>
		/// Gets pointer to certain row
		/// </summary>
		/// <param name="y">Position in y in data array coordinate system</param>s
		/// <returns>Pointer to row</returns>
		const_pointer operator[](coordinates_type y) const
		{
			if (y >= _localData.height)
				THROW_ARGUMENT_OUT_OF_RANGE(y);

			return getPtr(0, y);
		}

		/// <summary>
		/// Gets certain element by its position
		/// </summary>
		/// <param name="x">X coordinate in data array coordinate system</param>
		/// <param name="y">Y coordinate in data array coordinate system</param>
		/// <returns></returns>
		_Ty operator ()(coordinates_type x, coordinates_type y) const
		{
#if DEBUG || SAFE_INDEXING
			if (x >= width_elements())
				THROW_ARGUMENT_OUT_OF_RANGE(x);

			if (y >= _localData.height)
				THROW_ARGUMENT_OUT_OF_RANGE(y);
#endif

			return *getPtr(x, y);
		}

		/// <summary>
		/// Gets certain element by its position
		/// </summary>
		/// <param name="x">X coordinate in data array coordinate system</param>
		/// <param name="y">Y coordinate in data array coordinate system</param>
		/// <returns></returns>
		_Ty& operator ()(coordinates_type x, coordinates_type y)
		{
#if DEBUG || SAFE_INDEXING
			if (x >= width_elements())
				THROW_ARGUMENT_OUT_OF_RANGE(x);

			if (y >= _localData.height)
				THROW_ARGUMENT_OUT_OF_RANGE(y);
#endif

			return *getPtr(x, y);
		}

		/// <summary>
		/// Gets view of this image
		/// </summary>
		/// <returns>Full range of this image</returns>
		Image view() const
		{
			return Image<_Ty, _formatProvider>(
				_localData.ptr,
				{ _localData.width, _localData.height },
				_localData.step,
				_memoryManager);
		}

		/// <summary>
		/// Creates deep copy of this image
		/// </summary>
		/// <returns>Clone of this image</returns>
		Image clone() const
		{
			if (_memoryManager == nullptr) //If memory manager exists there is a guarantee that allocator is not null
			{
				if (_localData.ptr == nullptr) // the other image is empty - hence we create empty image
				{
					return Image(); // return empty Image
				}
				THROW_EXCEPTION("Static image cannot be cloned.");
			}

			auto image = Image(
				size_pixels(),
				_memoryManager->allocator());

			_memoryManager->copy(
				this->_localData,
				image._localData);
			return image;
		}

		/// <summary>
		/// Fills this image with provided value
		/// </summary>
		/// <param name="value"></param>
		void fill(_Ty value)
		{
			// Skip fill operation when image empty
			if (empty())
				return;

			if (_memoryManager != nullptr)
			{
				_memoryManager->fill(
					_localData,
					value);
			}
			else
			{
				fill_data(_localData, value);
			}
		}

		/// <summary>
		/// Gets range of this image, 
		/// !!! range has now R/W permissions
		/// </summary>
		/// <param name="x">pixel x</param>
		/// <param name="y">pixel y</param>
		/// <param name="width">width pixels</param>
		/// <param name="height">height pixels</param>
		/// <returns>New range</returns>
		Image range(
			coordinates_type pixelX,
			coordinates_type pixelY,
			coordinates_type widthPixels,
			coordinates_type heightPixels) const
		{
			AssertRangeIsValid(pixelX, pixelY, widthPixels, heightPixels);

			return Image<_Ty, _formatProvider>(
				getPtr(pixelX * _formatProvider::channels(), pixelY),
				{ widthPixels, heightPixels },
				_localData.step,
				_memoryManager);
		}

		/// <summary>
		/// Gets STATIC range of this image, 
		/// Static range is a simple view of the image and does not extend object live time as does range method.
		/// Be careful and ensure that the master object will live at least as long as static range
		/// </summary>
		/// <param name="x">pixel x</param>
		/// <param name="y">pixel y</param>
		/// <param name="width">width pixels</param>
		/// <param name="height">height pixels</param>
		/// <returns>New range</returns>
		Image range_s(
			coordinates_type pixelX,
			coordinates_type pixelY,
			coordinates_type widthPixels,
			coordinates_type heightPixels) const
		{
			AssertRangeIsValid(pixelX, pixelY, widthPixels, heightPixels);

			return Image<_Ty, _formatProvider>(
				getPtr(pixelX * _formatProvider::channels(), pixelY),
				{ widthPixels, heightPixels },
				_localData.step);
		}


		/// <summary>
		/// Gets range of this image
		/// !!! range has now R/W permissions
		/// </summary>
		/// <param name="rectPixels">rect to extract in pixels</param>
		/// <returns>New range</returns>
		Image range(
			const rect_type& rectPixels) const
		{
			return range(rectPixels.x, rectPixels.y, rectPixels.width, rectPixels.height);
		}

		/// <summary>
		/// Gets STATIC range of this image, 
		/// Static range is a simple view of the image and does not extend object live time as does range method.
		/// Be careful and ensure that the master object will live at least as long as static range
		/// </summary>
		/// <param name="rectPixels">rect to extract in pixels</param>
		/// <returns>New range</returns>
		Image range_s(
			const rect_type& rectPixels) const
		{
			return range_s(rectPixels.x, rectPixels.y, rectPixels.width, rectPixels.height);
		}

		/// <summary>
		/// Default constructor
		/// </summary>
		Image() :
			_localData(),
			_memoryManager(nullptr)
		{
		}

		/// <summary>
		/// Move constructor
		/// </summary>
		/// <param name="other">R-value object</param>
		Image(Image&& other) noexcept :
			_localData(std::move(other._localData)),
			_memoryManager(std::move(other._memoryManager))
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="sizePixels">size of the memory in pixels</param>
		/// <param name="allocator">allocator</param>
		Image(
			const size2d_type& sizePixels,
			allocator_pointer allocator)
		{
			ASSERT_NOT_NULL(allocator);
			_memoryManager = std::make_shared<ImageMemoryManager<_Ty>>(std::move(allocator));

			_localData = _memoryManager->alloc(
				sizePixels,
				channels());
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="dataHandle">pointer to the memory</param>
		/// <param name="sizePixels">size of the memory in pixels</param>
		/// <param name="step">step of the memory</param>
		Image(
			typename _formatProvider::pointer dataHandle,
			const size2d_type& sizePixels,
			size_type step)
			:
			_localData(
				dataHandle,
				sizePixels.width,
				sizePixels.height,
				_formatProvider::channels(),
				step),
			_memoryManager(nullptr)
		{
		}

		Image(const Image& other) = delete;
		Image& operator = (const Image& other) = delete;

		/// <summary>
		/// Move assignment operator
		/// </summary>
		Image& operator = (Image&& other) noexcept
		{
			if (&other == this)
			{
				return *this;
			}
			_memoryManager = std::move(other._memoryManager);
			_localData = std::move(other._localData);
			return *this;
		};

		/// <summary>
		/// Copies pixels from the src image if both images have the same size
		/// </summary>
		/// <param name="src">Image data are copied from</param>
		void copy_from(const Image& src)
		{
			// Skip copying when both data images are empty
			if (empty() && src.empty())
				return;

			if (this->size_pixels() != src.size_pixels())
				THROW_ARGUMENT_EXCEPTION(src, "Cannot copy from image of a different size.");

			if (_memoryManager != nullptr)
			{
				_memoryManager->copy(
					src._localData,
					this->_localData);

			}
			else
			{
				copy_data_2d<_Ty>(
					src._localData,
					this->_localData);
			}
		}

		/// <summary>
		/// Resizes the image
		/// </summary>
		void resize(const size2d_type& sizePixels)
		{
			if (size_pixels() == sizePixels)
				return;

			if (_memoryManager == nullptr)
			{
				INVALID_OPERATION_EXCEPTION("Cannot resize static object.");
			}

			if (_memoryManager.use_count() > 1)//unpinning the class
				_memoryManager = std::make_shared<ImageMemoryManager<_Ty>>(_memoryManager->allocator());
			else
				_memoryManager->dealloc();

			_localData = _memoryManager->alloc(
				sizePixels,
				channels());
		}

	protected:

		/// <summary>
		/// Image data
		/// </summary>
		ImageData<_Ty> _localData;

		/// <summary>
		/// Memory manager
		/// </summary>
		std::shared_ptr<ImageMemoryManager<_Ty>> _memoryManager;

		inline _Ty* getPtr(coordinates_type x, coordinates_type y) const
		{
			return calculate_ptr<_Ty>(_localData.ptr, x, y, _localData.step);
		}

		void AssertRangeIsValid(size_t pixelX, size_t pixelY, size_t widthPixels, size_t heightPixels) const
		{
			if (widthPixels == 0 || heightPixels == 0
				|| pixelX + widthPixels > _localData.width
				|| pixelY + heightPixels > _localData.height)
			{
				std::stringstream message;
				message << "Range {" << pixelX << ", " << pixelY << ", " << widthPixels << ", " << heightPixels <<
					"} exceeds image of size {" << _localData.width << "," << _localData.height << "}";
				THROW_EXCEPTION(message.str());
			}
		}

	private:

		/// <summary>
		/// Produces image based on memory range from other image
		/// </summary>
		/// <param name="roiPixels">Area of image</param>
		/// <param name="memoryManager">Memory manager</param>
		Image(
			typename _formatProvider::pointer dataHandle,
			const size2d_type& sizePixels,
			size_type step,
			std::shared_ptr<ImageMemoryManager<_Ty>> memoryManager)
			:
			_localData(dataHandle, sizePixels.width, sizePixels.height, _formatProvider::channels(), step),
			_memoryManager(std::move(memoryManager))
		{
		}
	};

	using Image_8u = Image<uint8_t>;
	using Image_16u = Image<uint16_t>;
	using Image_16s = Image<int16_t>;
	using Image_32s = Image<int32_t>;
	using Image_32f = Image<float>;

	using Image_8u_C3 = Image<uint8_t, fmt::C3Format<uint8_t>>;
	using Image_32f_C3 = Image<float, fmt::C3Format<float>>;

#define ASSERT_IMAGE_SIZES_EQUAL(img1, img2) if((img1).size_pixels() != (img2).size_pixels()) THROW_ARGUMENT_EXCEPTION(img1, "Sizes of images cannot differ.")
#define ASSERT_IMAGE_NOT_EMPTY(img) if((img).empty()) THROW_ARGUMENT_EXCEPTION(img, "Image cannot be empty")

}

#endif