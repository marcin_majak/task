#pragma once
#ifndef C3Format_H
#define C3Format_H

#include "ImageData.h"
#include "BaseDataFormatProvider.h"

namespace core
{
	namespace fmt
	{
		/// <summary>
		/// Base format for all RGB channel types
		/// </summary>
		template<typename _Ty>
		struct C3Format : BaseDataFormatProvider<_Ty, 3>
		{
			using ConstFormat = C3Format<const _Ty>;
			using pointer = typename ImageData<_Ty>::pointer;
			using const_pointer = const pointer;
			using channel_count_type = size_t;

			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="data">image data view</param>
			C3Format(const ImageData<_Ty>& data) :
				BaseDataFormatProvider(data)
			{
			}

			/// <summary>
			/// Gets channel by offset
			/// </summary>
			/// <returns>pointer to channel of certain</returns>
			pointer channel(int offset) { return this->_data.ptr + offset; }

			/// <summary>
			/// Gets channel by offset
			/// </summary>
			/// <returns>pointer to channel of certain nr</returns>
			const_pointer channel(int offset) const { return this->_data.ptr + offset; }
		};
	}
}

#endif
