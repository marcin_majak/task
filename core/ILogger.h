#pragma once 
#ifndef ILogger_H
#define ILogger_H

#include "ExportDefines.h"
#include <memory>
#include <string>

namespace core
{
	struct CORE_API ILogger
	{
		virtual ~ILogger();

		virtual void Log(std::string text) const = 0;
	};

	std::unique_ptr<ILogger> CORE_API CreateConsoleLogger();
}

#endif