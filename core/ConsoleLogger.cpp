#include "ConsoleLogger.h"
#include <iostream>

namespace core
{
	void ConsoleLogger::Log(std::string text) const
	{
		std::cout << text << std::endl;
	}
}