#include "Image.h"
#include "ExportDefines.h"

namespace core
{
	template struct CORE_API ImageData<bool>;
	template struct CORE_API ImageData<uint8_t>;
	template struct CORE_API ImageData<uint16_t>;
	template struct CORE_API ImageData<int16_t>;
	template struct CORE_API ImageData<int32_t>;
	template struct CORE_API ImageData<float>;

	template class CORE_API std::shared_ptr<IImageAllocator<bool>>;
	template class CORE_API std::shared_ptr<IImageAllocator<uint8_t>>;
	template class CORE_API std::shared_ptr<IImageAllocator<uint16_t>>;
	template class CORE_API std::shared_ptr<IImageAllocator<int16_t>>;
	template class CORE_API std::shared_ptr<IImageAllocator<int32_t>>;
	template class CORE_API std::shared_ptr<IImageAllocator<float>>;

	template class CORE_API std::shared_ptr<ImageMemoryManager<bool>>;
	template class CORE_API std::shared_ptr<ImageMemoryManager<uint8_t>>;
	template class CORE_API std::shared_ptr<ImageMemoryManager<uint16_t>>;
	template class CORE_API std::shared_ptr<ImageMemoryManager<int16_t>>;
	template class CORE_API std::shared_ptr<ImageMemoryManager<int32_t>>;
	template class CORE_API std::shared_ptr<ImageMemoryManager<float>>;

	template struct CORE_API fmt::BaseDataFormatProvider<bool, 1>;
	template struct CORE_API fmt::BaseDataFormatProvider<uint8_t, 1>;
	template struct CORE_API fmt::BaseDataFormatProvider<uint8_t, 3>;
	template struct CORE_API fmt::BaseDataFormatProvider<uint16_t, 1>;
	template struct CORE_API fmt::BaseDataFormatProvider<int16_t, 1>;
	template struct CORE_API fmt::BaseDataFormatProvider<int32_t, 1>;
	template struct CORE_API fmt::BaseDataFormatProvider<float, 1>;
	template struct CORE_API fmt::BaseDataFormatProvider<float, 3>;

	template struct CORE_API fmt::GrayScaleFormat<bool>;
	template struct CORE_API fmt::GrayScaleFormat<uint8_t>;
	template struct CORE_API fmt::GrayScaleFormat<uint16_t>;
	template struct CORE_API fmt::GrayScaleFormat<int16_t>;
	template struct CORE_API fmt::GrayScaleFormat<int32_t>;
	template struct CORE_API fmt::GrayScaleFormat<float>;
	template struct CORE_API fmt::C3Format<uint8_t>;
	template struct CORE_API fmt::C3Format<float>;

	template class CORE_API Image<bool>;
	template class CORE_API Image<uint8_t>;
	template class CORE_API Image<uint16_t>;
	template class CORE_API Image<int16_t>;
	template class CORE_API Image<int32_t>;
	template class CORE_API Image<float>;
	template class CORE_API Image<uint8_t, fmt::C3Format<uint8_t>>;
	template class CORE_API Image<float, fmt::C3Format<float>>;
}