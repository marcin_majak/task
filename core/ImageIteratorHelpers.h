#pragma once

#ifndef ImageIteratorHelpers_H
#define ImageIteratorHelpers_H
#include "ImageData.h"
#include "ImageIterator.h"

namespace core
{
	template<typename _Ty, typename _Tu>
	inline image_iterator<_Ty> create_iterator(
		_Ty* channelPtr,
		const ImageData<_Tu>& data,
		size_t x,
		size_t y,
		size_t channelStride = 1)
	{
		return image_iterator<_Ty>(
			calculate_ptr(channelPtr, x, y, data.step),
			x, y,
			data.width * data.channels,
			data.step,
			channelStride);
	}

	template<typename _Ty, typename _Tu>
	inline image_iterator<_Ty> create_begin_iterator(
		_Ty* channelPtr,
		const ImageData<_Tu>& data)
	{
		return create_iterator<_Ty, _Tu>(channelPtr, data, 0, 0, 1);
	}

	template<typename _Ty, typename _Tu>
	inline image_iterator<_Ty> create_end_iterator(
		_Ty* channelPtr,
		const ImageData<_Tu>& data)
	{
		return create_iterator<_Ty, _Tu>(channelPtr, data, 0, data.height, 1);
	}

	template<typename _Ty, typename _Tu>
	inline image_iterator<_Ty> create_begin_channel_iterator(
		_Ty* channelPtr,
		const ImageData<_Tu>& data)
	{
		return create_iterator<_Ty, _Tu>(channelPtr, data, 0, 0, data.channels);
	}

	template<typename _Ty, typename _Tu>
	inline image_iterator<_Ty> create_end_channel_iterator(
		_Ty* channelPtr,
		const ImageData<_Tu>& data)
	{
		return create_iterator<_Ty, _Tu>(channelPtr, data, 0, data.height, data.channels);
	}

}
#endif