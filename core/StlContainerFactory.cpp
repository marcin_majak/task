#include "StlContainerFactory.h"

namespace core
{
	const IGenericContainerFactory<bool>& StlContainerFactory::Create_bool() const
	{
		return factoryFor_bool;
	}

	const IGenericContainerFactory<unsigned char>& StlContainerFactory::Create_8u() const
	{
		return factoryFor_8u;
	}

	const IGenericContainerFactory<unsigned short>& StlContainerFactory::Create_16u() const
	{
		return factoryFor_16u;
	}

	const IGenericContainerFactory<short>& StlContainerFactory::Create_16s() const
	{
		return factoryFor_16s;
	}

	const IGenericContainerFactory<int>& StlContainerFactory::Create_32s() const
	{
		return factoryFor_32s;
	}

	const IGenericContainerFactory<float>& StlContainerFactory::Create_32f() const
	{
		return factoryFor_32f;
	}

	StlContainerFactory::~StlContainerFactory() = default;

	std::unique_ptr<IContainerFactory> CreateStlContainerFactory()
	{
		return std::make_unique<StlContainerFactory>();
	}

}