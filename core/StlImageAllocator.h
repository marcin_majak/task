#pragma once

#ifndef StlImageAllocator_H
#define StlImageAllocator_H

#include "IImageAllocator.h"
#include "ImageData.h"
#include "ImageDataHelpers.h"

namespace core
{
	/// <summary>
	/// Allocator class for stl-like data format
	/// </summary>
	template<typename _Ty>
	struct StlImageAllocator : IImageAllocator<_Ty>
	{
		ImageData<_Ty> alloc(
			const Size& sizePixels,
			const size_t channels) const override
		{
			if (sizePixels.width == 0 || sizePixels.height == 0)
				return ImageData<_Ty>(nullptr, sizePixels.width, sizePixels.height, channels, 0);

			return alloc_data<_Ty>(sizePixels, channels);
		}

		void copy(
			const ImageData<_Ty>& src,
			ImageData<_Ty>& dst) const override
		{
			copy_data_2d(src, dst);
		}

		void dealloc(_Ty* data) const override
		{
			delete[] data;
		}

		void fill(
			ImageData<_Ty>& data,
			const _Ty value) const override
		{
			fill_data(data, value);
		}
	};

}
#endif