#pragma once
#ifndef GrayScaleFormat_H
#define GrayScaleFormat_H

#include "ImageData.h"
#include "BaseDataFormatProvider.h"

namespace core
{
	namespace fmt
	{
		/// <summary>
		/// Format for GrayScale images	
		/// </summary>
		template<typename _Ty>
		struct GrayScaleFormat : BaseDataFormatProvider<_Ty, 1>
		{
			using ConstFormat = GrayScaleFormat<const _Ty>;
			using pointer = typename ImageData<_Ty>::pointer;
			using const_pointer = const pointer;
			using channel_count_type = size_t;

			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="data">image data</param>
			GrayScaleFormat(
				const ImageData<_Ty>& data) :
				BaseDataFormatProvider(data) {}

			/// <summary>
			/// Gets Gray channel pointer
			/// </summary>
			/// <returns>Gray pointer</returns>
			pointer gray() { return this->_data.ptr; }

			/// <summary>
			/// Gets Gray channel pointer
			/// </summary>
			/// <returns>Gray pointer</returns>
			const_pointer gray() const { return this->_data.ptr; }
		};
	}
}

#endif