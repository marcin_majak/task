#pragma once

#ifndef CoreHelpers_H
#define CoreHelpers_H

#include <string>

namespace core
{
	template<class ClassType>
	std::string get_class_name()
	{
		std::string type = typeid(ClassType).name();

		auto pos = type.find_last_of(':');
		if (pos == type.npos)
			return type;

		return type.substr(pos + 1, type.size() - pos - 1);
	}
}

#endif