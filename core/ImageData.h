#pragma once

#ifndef ImageData_H
#define ImageData_H

namespace core
{
	/// <summary>
	/// Image data container
	/// </summary>
	template <typename _Ty>
	struct ImageData
	{
		using pointer = _Ty *;
		using const_pointer = const pointer;

		/// <summary>
		/// Data pointer
		/// </summary>
		pointer ptr = nullptr;

		/// <summary>
		/// Data width pixels
		/// </summary>
		size_t width = 0;

		/// <summary>
		/// Height of the data
		/// </summary>
		size_t height = 0;

		/// <summary>
		/// Number of channels
		/// </summary>
		size_t channels = 0;

		/// <summary>
		/// Data step
		/// </summary>
		size_t step = 0;

		/// <summary>
		/// Default constructor
		/// </summary>
		ImageData() = default;

		/// <summary>
		/// Copy constructor
		/// </summary>
		/// <param name="other">other object of the same type</param>
		ImageData(const ImageData& other) = default;

		ImageData(
			pointer data,
			size_t widthPixels,
			size_t height,
			size_t channels,
			size_t step) :
			ptr(data),
			width(widthPixels),
			height(height),
			channels(channels),
			step(step)
		{}

		ImageData(ImageData&& other) noexcept :
			ptr(other.ptr),
			width(other.width),
			height(other.height),
			channels(other.channels),
			step(other.step)
		{
			other.ptr = nullptr;
			other.width = 0;
			other.height = 0;
			other.channels = 0;
			other.step = 0;
		}

		ImageData& operator = (const ImageData& other) = default;
	};

}

#endif