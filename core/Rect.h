#pragma once

#ifndef Rect_H
#define Rect_H

#include <type_traits>
#include "EqualityHelpers.h"
#include "Point.h"
#include "Size.h"
#include "ExportDefines.h"

namespace core
{
	template<typename _Tp, typename = typename std::enable_if<std::is_arithmetic<_Tp>::value>::type>
	class Rect_
	{
	public:

		Rect_() : x(0), y(0), width(0), height(0)
		{

		}

		Rect_(const Point_<_Tp>& topLeft, const Size_<_Tp>& size) :
			Rect_(topLeft.x, topLeft.y, size.width, size.height)
		{

		}

		Rect_(_Tp _x, _Tp _y, _Tp _width, _Tp _height) : x(_x), y(_y), width(_width), height(_height)
		{

		}

		Size_<_Tp> size() const
		{
			return Size_<_Tp>{width, height};
		}

		template<class _AreaType = _Tp>
		_AreaType area() const
		{
			return static_cast<_AreaType>(width) * static_cast<_AreaType>(height);
		}

		bool operator==(const Rect_& right) const
		{
			return this->x == right.x && this->y == right.y && this->width == right.width && this->height == right.height;
		}

		bool operator!=(const Rect_& right) const
		{
			return !(*this == right);
		}

		Point_<_Tp> top_left() const
		{
			return Point_<_Tp>(x, y);
		}

		Point_<_Tp> bottom_right() const
		{
			return Point_<_Tp>(x + width - 1, y + height - 1);
		}

		bool is_inside(const Rect_& other) const
		{
			auto topLeft = top_left();
			auto otherTopLeft = other.top_left();

			if (topLeft.x < otherTopLeft.x || topLeft.y < otherTopLeft.y)
				return false;

			auto bottomRight = bottom_right();
			auto otherBottomRight = other.bottom_right();

			return !static_cast<bool>(bottomRight.x > otherBottomRight.x || bottomRight.y > otherBottomRight.y);
		}


		/// <summary> The top left x coordinate </summary>
		_Tp x;

		/// <summary> A top left y coordinate </summary>
		_Tp y;

		/// <summary> The width of rect. </summary>
		_Tp width;

		/// <summary> The height of rectangle </summary>
		_Tp height;
	};

	template<>
	inline bool Rect_<float>::operator==(const Rect_<float>& right) const
	{
		return equal_epsilon(x, right.x) &&
			equal_epsilon(y, right.y) &&
			equal_epsilon(width, right.width) &&
			equal_epsilon(height, right.height);
	}

	template<>
	inline bool Rect_<float>::operator!=(const Rect_<float>& right) const
	{
		return !(*this == right);
	}

	template<>
	inline bool Rect_<double>::operator==(const Rect_<double>& right) const
	{
		return equal_epsilon(x, right.x) &&
			equal_epsilon(y, right.y) &&
			equal_epsilon(width, right.width) &&
			equal_epsilon(height, right.height);
	}

	template<>
	inline bool Rect_<double>::operator!=(const Rect_<double>& right) const
	{
		return !(*this == right);
	}

	using Rect2i = Rect_<int>;
	using Rect2ui = Rect_<unsigned int>;
	using Rect2ull = Rect_<size_t>;
	using Rect2f = Rect_<float>;
	using Rect2d = Rect_<double>;
	using Rect = Rect2i;

	template class CORE_API Rect_<int>;
	template class CORE_API Rect_<unsigned int>;
	template class CORE_API Rect_<size_t>;
	template class CORE_API Rect_<float>;
	template class CORE_API Rect_<double>;

}

#endif