#pragma once

#ifndef ImageMemoryHelpers_H
#define ImageMemoryHelpers_H

#include <cstdint>
#include "ConstTraits.h"

namespace core
{
	template<typename T>
	inline T* alloc_array(
		size_t widthPixels,
		size_t heightPixels,
		size_t channels)
	{
		return new T[widthPixels * channels * heightPixels];
	}

	template<typename T>
	inline T* end_line_ptr(
		T* lineBeginPtr,
		size_t widthPixels,
		size_t channels)
	{
		return lineBeginPtr + widthPixels * channels;
	}

	template<class T>
	inline T* calculate_ptr(T* ptr, size_t x, size_t y, size_t step)
	{
		return reinterpret_cast<T*>(reinterpret_cast<typename PassConst<T, uint8_t>::type*>(ptr) + y * step) + x;
	}

	template<class T>
	inline T* add_bytes_to_ptr(T* ptr, size_t bytes)
	{
		return reinterpret_cast<T*>(reinterpret_cast<typename PassConst<T, uint8_t>::type*>(ptr) + bytes);
	}
}
#endif
