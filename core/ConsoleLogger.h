#pragma once 

#ifndef ConsoleLogger_H
#define ConsoleLooger_H

#include "ILogger.h"

namespace core
{
	class ConsoleLogger : public ILogger
	{
	public:

		void Log(std::string text) const override;
	};
}

#endif