#pragma once

#ifndef EqualityHelpers_H
#define EqualityHelpers_H

#include <cmath>
#include <limits>

namespace core
{
	/// <summary> The function checks if the two function are equal with epsilon. The epsilon is defines as std::numeric_limits[T]::epsilon() </summary>
	/// <typeparam name="T"> Generic type parameter. </typeparam>
	/// <param name="src"> Source for the. </param>
	/// <param name="dst"> Destination for the. </param>
	/// <returns> True if it succeeds, false if it fails. </returns>
	template<class T>
	inline bool equal_epsilon(T src, T dst)
	{
		return std::abs(src - dst) <= std::numeric_limits<T>::epsilon();
	}
}

#endif