#pragma once

#ifndef CoreTraits_H
#define CoreTraits_H
#include <type_traits>

namespace core
{
	/// <summary>
	/// Traits required to handle functions differ with only const before ptr
	/// Adds const to out type if in type contains const
	/// </summary>
	template<class _TIn, class _TOut, bool = std::is_const<_TIn>::value>
	struct PassConst
	{
		typedef _TOut type;
	};

	/// <summary>
	/// Situation when in is without const
	/// </summary>
	template<class _TIn, class _TOut>
	struct PassConst<_TIn, _TOut, false>
	{
		typedef _TOut type;
	};

	/// <summary>
	/// Situation when in has const
	/// </summary>
	template<class _TIn, class _TOut>
	struct PassConst<_TIn, _TOut, true>
	{
		typedef const _TOut type;
	};
}

#endif