#pragma once

#ifndef IImageAllocator_H
#define IImageAllocator_H

#include "ImageData.h"
#include "Size.h"

namespace core
{
	/// <summary>
	/// Allocator interface
	/// </summary>
	template<typename _Ty>
	struct IImageAllocator
	{
		virtual ~IImageAllocator() = default;

		virtual ImageData<_Ty> alloc(
			const Size& sizePixels,
			const size_t channels) const = 0;

		virtual void copy(
			const ImageData<_Ty>& src,
			ImageData<_Ty>& dst) const = 0;

		virtual void dealloc(
			_Ty* data) const = 0;

		virtual void fill(
			ImageData<_Ty>& data,
			const _Ty value) const = 0;
	};
}

#endif
