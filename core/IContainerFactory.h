#pragma once

#ifndef IContainerFactory_H
#define IContainerFactory_H

#include <cstdint>

#include "Image.h"

namespace core
{
	template<class T>
	class IGenericContainerFactory
	{
	public:
		virtual ~IGenericContainerFactory() = default;

		/// <summary> Function creates an image of given size. </summary>
		/// <param name="memoryPtr"> The pointer to memory where the image is located. </param>
		/// <param name="width"> The width of image. </param>
		/// <param name="height"> The height of image. </param>
		/// <param name="step"> The size of image row in bytes. 
		/// 					This value is added to beginning of the row to acces the next row</param>
		/// <returns> A static image initialized with given memory</returns>
		virtual core::Image<T> Image(T* memoryPtr, size_t width, size_t height, size_t step) const = 0;

		/// <summary> Function creates an 3-channel image of given size. </summary>
		/// <param name="memoryPtr"> The pointer to memory where the image is located. </param>
		/// <param name="width"> The width of image. </param>
		/// <param name="height"> The height of image. </param>
		/// <param name="step"> The size of image row in bytes. 
		/// 					This value is added to beginning of the row to acces the next row</param>
		/// <returns> A static image initialized with given memory</returns>
		virtual core::Image<T, fmt::C3Format<T>> Image_C3(T* memoryPtr, size_t width, size_t height, size_t step) const = 0;

		/// <summary> Function creates an image object of given size </summary>
		/// <param name="width">  The width of image. </param>
		/// <param name="height"> The height of image. </param>
		/// <returns> An image object </returns>
		virtual core::Image<T> Image(size_t width, size_t height) const = 0;

		/// <summary> Function creates an 3-channel image object of given size </summary>
		/// <param name="width">  The width of image. </param>
		/// <param name="height"> The height of image. </param>
		/// <returns> An image object </returns>
		virtual core::Image<T, fmt::C3Format<T>> Image_C3(size_t width, size_t height) const = 0;

		/// <summary> Function creates an 3-channel image object of given size </summary>
		/// <param name="size"> The image size. </param>
		/// <returns> An image object </returns>
		virtual core::Image<T, fmt::C3Format<T>> Image_C3(const core::Size& size) const = 0;

		/// <summary> Function creates an image object of given size. </summary>
		/// <param name="size"> The image size. </param>
		/// <returns> An image object. </returns>
		virtual core::Image<T> Image(const core::Size& size) const = 0;
	};

	template<class T>
	class GenericContainerFactory : public IGenericContainerFactory<T> {
	public:
		virtual ~GenericContainerFactory() = default;
		core::Image<T> Image(T* memoryPtr, size_t width, size_t height, size_t step) const override {
			return core::Image<T>(memoryPtr, Size(width, height), step);
		}

		core::Image<T, fmt::C3Format<T>> Image_C3(T* memoryPtr, size_t width, size_t height, size_t step) const override {
			return core::Image<T, fmt::C3Format<T>>(memoryPtr, Size(width, height), step);
		}
	};

	class CORE_API IContainerFactory
	{
	public:
		virtual ~IContainerFactory() = default;

		/// <summary> Access the factory of containers of bool type. The user should store the reference IGenericContainerFactory! </summary>
		/// <returns> The factory for bool containers </returns>
		virtual const IGenericContainerFactory<bool>& Create_bool() const = 0;

		/// <summary> Access the factory of containers of 8u type. The user should store the reference IGenericContainerFactory! </summary>
		/// <returns> The factory for 8u containers </returns>
		virtual const IGenericContainerFactory<uint8_t>& Create_8u() const = 0;

		/// <summary> Access the factory of containers of 16u type. The user should store the reference IGenericContainerFactory! </summary>
		/// <returns> The factory for 16u containers </returns>
		virtual const IGenericContainerFactory<uint16_t>& Create_16u() const = 0;

		/// <summary> Access the factory of containers of 16s type. The user should store the reference IGenericContainerFactory! </summary>
		/// <returns> The factory for 16s containers </returns>
		virtual const IGenericContainerFactory<int16_t>& Create_16s() const = 0;

		/// <summary> Access the factory of containers of 32s type. The user should store the reference IGenericContainerFactory! </summary>
		/// <returns> The factory for 32s containers </returns>
		virtual const IGenericContainerFactory<int32_t>& Create_32s() const = 0;

		/// <summary> Access the factory of containers of 32f type. The user should store the reference IGenericContainerFactory! </summary>
		/// <returns> The factory for 32f containers </returns>
		virtual const IGenericContainerFactory<float>& Create_32f() const = 0;

		/// <summary> The functions provides an access to container factory of given type. The user should store the reference IGenericContainerFactory! </summary>
		/// <typeparam name="T"> The type of container to be created. </typeparam>
		/// <returns> A container factory to create Image or signal of given type </returns>
		template<class T>
		const IGenericContainerFactory<T>& Create() const
		{
			// ReSharper disable once CppStaticAssertFailure
			static_assert(false, "The function does not provide a specialization for this type.");
			return {};
		}
	};

	template<>
	inline const IGenericContainerFactory<bool>& IContainerFactory::Create<bool>() const
	{
		return this->Create_bool();
	}

	template<>
	inline const IGenericContainerFactory<uint8_t>& IContainerFactory::Create<uint8_t>() const
	{
		return this->Create_8u();
	}

	template<>
	inline const IGenericContainerFactory<uint16_t>& IContainerFactory::Create<uint16_t>() const
	{
		return this->Create_16u();
	}

	template<>
	inline const IGenericContainerFactory<int16_t>& IContainerFactory::Create<int16_t>() const
	{
		return this->Create_16s();
	}

	template<>
	inline const IGenericContainerFactory<int32_t>& IContainerFactory::Create<int32_t>() const
	{
		return this->Create_32s();
	}

	template<>
	inline const IGenericContainerFactory<float>& IContainerFactory::Create<float>() const
	{
		return this->Create_32f();
	}
}

#endif