#pragma once

#ifndef IOWrapperInterface_H
#define IOWrapperInterface_H

#include <Image.h>
#include <string>
#include "ExportDefines.h"

namespace io
{
	struct IO_API IOWrapperInterface
	{
		~IOWrapperInterface();

		virtual core::Image_8u_C3 Read(std::string path) = 0;
		
		virtual void Write(const core::Image_8u_C3& image, std::string path) = 0;
		
		virtual void Write(const core::Image_8u& image, std::string path) = 0;
	};

	std::unique_ptr<IOWrapperInterface> IO_API CreateIOWrapper();
}

#endif