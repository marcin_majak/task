#include "IOWrapperInterface.h"
#include "IOWrapper.h"

namespace io
{
	IOWrapperInterface::~IOWrapperInterface() = default;

	std::unique_ptr<IOWrapperInterface> CreateIOWrapper()
	{
		return std::make_unique<IOWrapper>();
	}
}