#include "IOWrapper.h"
#include <opencv2/highgui/highgui.hpp>

namespace io
{
	core::Image_8u_C3 IOWrapper::Read(std::string path)
	{
		cv::Mat image = imread(path, cv::IMREAD_COLOR);
		core::Image_8u_C3 result(core::Size(image.cols, image.rows), 
			std::make_shared<core::StlImageAllocator<uint8_t>>());

		if (result.step() == image.step)
		{
			// unified memory
			std::memcpy(result.ptr(), image.data, result.height() * result.step());
		}
		else
		{
			// copy row by row
			for (auto y = 0; y < image.rows; ++y)
			{
				std::memcpy(result.ptr(0, y), image.ptr(y), result.width() * result.channels());
			}
		}

		return result;
	}

	void IOWrapper::Write(const core::Image_8u_C3& image, std::string path)
	{
		Write(const_cast<uint8_t*>(image.ptr()),
			int(image.width()), int(image.height()),
			int(image.step()),
			CV_8UC3,
			std::move(path));
	}

	void IOWrapper::Write(const core::Image_8u& image, std::string path)
	{
		Write(const_cast<uint8_t*>(image.ptr()),
			int(image.width()), int(image.height()), 
			int(image.step()), 
			CV_8UC1, 
			std::move(path));
	}

	void IOWrapper::Write(uint8_t* ptr, int width, int height, int step, int cvType, std::string path)
	{
		const cv::Mat cvMat(height, width,
			cvType,
			ptr,
			step);

		if (!cv::imwrite(path, cvMat))
			THROW_EXCEPTION("Cannot save image into provided path");
	}
}
