#pragma once

#ifndef IO_ExportDefines_H
#define IO_ExportDefines_H

#ifdef IO_EXPORTS
#define IO_API __declspec(dllexport)
#else
#define IO_API __declspec(dllimport)
#endif

#endif