#pragma once

#ifndef IOWrapper_H
#define IOWrapper_H

#include "IOWrapperInterface.h"
#include "Image.h"

namespace io
{
	class IOWrapper : public IOWrapperInterface
	{
	public:
		core::Image_8u_C3 Read(std::string path) override;

		void Write(const core::Image_8u_C3& image, std::string path) override;

		void Write(const core::Image_8u& image, std::string path) override;

	private:
		void Write(uint8_t* ptr, int width, int height, int step, int cvType, std::string path);
	};
}
#endif