#pragma once

#ifndef IImageCalculator_H
#define IImageCalculator_H
#include <Image.h>
#include "ExportDefines.h"

namespace ip
{
	struct IP_API IImageCalculator
	{
		virtual ~IImageCalculator();

		virtual void Abs_I(core::Image_32f& srcDst) const = 0;

		virtual void Sub_I(core::Image_32f& srcDst, const core::Image_32f& subtrahend) const = 0;

		virtual void SetWithMask(core::Image_16u& srcDst, const core::Image<bool>& mask, uint16_t value) const = 0;

		virtual void CopyWithMask(const core::Image_32f& src, const core::Image<bool>& mask, core::Image_32f& dst) const = 0;
	};
}
#endif