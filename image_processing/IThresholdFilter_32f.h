#pragma once

#ifndef IThresholdFilter_32f_H
#define IThresholdFilter_32f_H
#include <Image.h>
#include "ExportDefines.h"

namespace ip
{
	struct IP_API IThresholdFilter_32f
	{
		virtual ~IThresholdFilter_32f();

		virtual void Process(const core::Image_32f& lhr, const core::Image_32f& rhr, core::Image<bool>& mask) const = 0;
	};
}
#endif 