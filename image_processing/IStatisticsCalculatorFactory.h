#pragma once

#ifndef IStatisticsCalculatorFactory_H
#define IStatisticsCalculatorFactory_H
#include <memory>
#include "IStatisticsCalculator.h"
#include "ExportDefines.h"

namespace ip
{
	struct IP_API IStatisticsCalculatorFactory
	{
		virtual ~IStatisticsCalculatorFactory();

		virtual std::unique_ptr<IStatisticsCalculator> CreateStatisticsCalculator() const = 0;
	};

	std::unique_ptr<IStatisticsCalculatorFactory> IP_API CreateStatisticsCalculatorFactory();
}
#endif