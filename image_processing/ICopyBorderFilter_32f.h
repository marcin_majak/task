#pragma once

#ifndef ICopyBorderFilter_32f_H
#define ICopyBorderFilter_32f_H

#include <Image.h>
#include "ExportDefines.h"

namespace ip
{
	struct IP_API ICopyBorderFilter_32f
	{
		virtual ~ICopyBorderFilter_32f();

		virtual core::Image_32f CopyBorder(const core::Image_32f& src, const core::Size& expectedSize) = 0;
	};
}

#endif