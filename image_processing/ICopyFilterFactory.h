#pragma once

#ifndef ICopyFilterFactory_H
#define ICopyFilterFactory_H
#include <memory>
#include "ICopyBorderFilter_32f.h"
#include <IContainerFactory.h>
#include "ExportDefines.h"

namespace ip
{
	struct IP_API ICopyFilterFactory
	{
		virtual ~ICopyFilterFactory();

		virtual std::unique_ptr<ICopyBorderFilter_32f> CreateCopyBorderFilter() = 0;
	};

	std::unique_ptr<ICopyFilterFactory> IP_API CreateCopyFilterFactory(std::shared_ptr<core::IContainerFactory> containerFactory);
}
#endif