#pragma once
#ifndef ImageProcessing_ExportDefines_H
#define ImageProcessing_ExportDefines_H

#ifdef IP_EXPORTING
#define IP_API __declspec(dllexport)
#else
#define IP_API __declspec(dllimport)
#endif

#endif