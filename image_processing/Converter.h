#pragma once

#ifndef Converter_H
#define Converter_H

#include "IConverter.h"

namespace ip
{
	class Converter : public IConverter
	{
		void Convert(const core::Image_8u_C3& src, core::Image_32f& dst) const override;

		void Convert(const core::Image_32f& src, float srcMinValue, float srcMaxValue, core::Image_8u& dst) const override;
	};
}
#endif