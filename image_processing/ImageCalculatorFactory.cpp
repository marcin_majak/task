#include "ImageCalculatorFactory.h"
#include "ImageCalculator.h"

namespace ip
{
	std::unique_ptr<IImageCalculator> ImageCalculatorFactory::CreateImageCalculator() const
	{
		return std::make_unique<ImageCalculator>();
	}
}
