#include "ICopyFilterFactory.h"
#include "CopyFilterFactory.h"

namespace ip
{
	ICopyFilterFactory::~ICopyFilterFactory() = default;

	std::unique_ptr<ICopyFilterFactory> CreateCopyFilterFactory(std::shared_ptr<core::IContainerFactory> containerFactory)
	{
		return std::make_unique<CopyFilterFactory>(std::move(containerFactory));
	}
}
