#include "ConverterFactory.h"
#include "Converter.h"

namespace ip
{
	std::unique_ptr<IConverter> ConverterFactory::CreateConverter()
	{
		return std::make_unique<Converter>();
	}
}
