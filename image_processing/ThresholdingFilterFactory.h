#pragma once

#ifndef ThresholdingFilterFactory_H
#define ThresholdingFilterFactory_H

#include "IThresholdingFilterFactory.h"

namespace ip
{
	class ThresholdingFilterFactory : public IThresholdingFilterFactory
	{
	public:
		std::unique_ptr<IThresholdFilter_32f> CreateLessThanThresholdFilter_32f() const override;
	};
}

#endif