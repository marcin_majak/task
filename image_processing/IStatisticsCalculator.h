#pragma once

#ifndef IStatisticsCalculator_H
#define IStatisticsCalculator_H

#include <tuple>
#include <Image.h>
#include "ExportDefines.h"

namespace ip
{
	struct IP_API IStatisticsCalculator
	{
		virtual ~IStatisticsCalculator();

		virtual std::tuple<float, float> GetMinMax(const core::Image_32f& src) const = 0;
	};
}

#endif 