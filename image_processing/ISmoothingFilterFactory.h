#pragma once

#ifndef ISmoothingFilterFactory_H
#define ISmoothingFilterFactory_H
#include <memory>
#include "IFilter_32f.h"
#include "ICopyFilterFactory.h"
#include "ExportDefines.h"

namespace ip
{
	struct IP_API ISmoothingFilterFactory
	{
		virtual ~ISmoothingFilterFactory();

		virtual std::unique_ptr<IFilter_32f> CreateGaussFilter(int windowSize) = 0;
	};

	std::unique_ptr<ISmoothingFilterFactory> IP_API CreateSmoothingFactory(std::shared_ptr<ip::ICopyFilterFactory> copyFilterFactory);
}
#endif