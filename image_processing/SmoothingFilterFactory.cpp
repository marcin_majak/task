#include "SmoothingFilterFactory.h"
#include "GaussFilter_32f.h"

namespace ip
{
	SmoothingFilterFactory::SmoothingFilterFactory(std::shared_ptr<ip::ICopyFilterFactory> copyFilterFactory) :
			_copyFilterFactory(std::move(copyFilterFactory))
	{
		ASSERT_NOT_NULL(_copyFilterFactory);
	}

	std::unique_ptr<IFilter_32f> SmoothingFilterFactory::CreateGaussFilter(int windowSize)
	{
		return std::make_unique<GaussFilter_32f>(windowSize, _copyFilterFactory->CreateCopyBorderFilter());
	}
}
