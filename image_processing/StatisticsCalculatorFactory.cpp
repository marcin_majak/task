#include "StatisticsCalculatorFactory.h"
#include "StatisticsCalculator.h"

namespace ip
{
	std::unique_ptr<IStatisticsCalculator> StatisticsCalculatorFactory::CreateStatisticsCalculator() const
	{
		return std::make_unique<StatisticsCalculator>();
	}
}
