#pragma once

#ifndef CopyBorderFilter_H
#define CopyBorderFilter_H

#include <memory>
#include <IContainerFactory.h>
#include "ICopyBorderFilter_32f.h"


namespace ip
{
	class CopyBorderFilter_32f : public ICopyBorderFilter_32f
	{
	public:
		CopyBorderFilter_32f(std::shared_ptr<core::IContainerFactory> containerFactory);

		core::Image_32f CopyBorder(const core::Image_32f& src, const core::Size& expectedSize) override;

	private:
		std::shared_ptr<core::IContainerFactory> _containerFactory;
	};
}
#endif
