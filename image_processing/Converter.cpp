#include "Converter.h"
#include <algorithm>

namespace ip
{
	void Converter::Convert(const core::Image_8u_C3& src, core::Image_32f& dst) const
	{
		ASSERT_IMAGE_NOT_EMPTY(src);
		ASSERT_IMAGE_SIZES_EQUAL(src, dst);

		const float rCoefficient = 0.2989f; 
		const float gCoefficient = 0.5870f;
		const float bCoefficient = 0.1140f;

		for (auto y=0; y<int(src.height()); ++y)
		{
			auto srcPtr = src.ptr(0, y);
			auto dstPtr = dst.ptr(0, y);
			for (auto x=0; x<int(src.width()); ++x)
			{
				*dstPtr = srcPtr[0] * bCoefficient + srcPtr[1] * gCoefficient + srcPtr[2] * rCoefficient;

				++dstPtr;
				srcPtr += src.channels();
			}
		}
	}

	void Converter::Convert(const core::Image_32f& src, float srcMinValue, float srcMaxValue, core::Image_8u& dst) const
	{
		ASSERT_IMAGE_NOT_EMPTY(src);
		ASSERT_IMAGE_SIZES_EQUAL(src, dst);

		std::transform(std::begin(src), std::end(src), std::begin(dst), [srcMinValue, srcMaxValue](const auto& value)
		{
			return static_cast<uint8_t>((value - srcMinValue) / (srcMaxValue - srcMinValue) * 255);
		});
	}
}
