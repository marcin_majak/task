#include "StatisticsCalculator.h"
#include <algorithm>

namespace ip
{
	std::tuple<float, float> StatisticsCalculator::GetMinMax(const core::Image_32f& src) const
	{
		ASSERT_IMAGE_NOT_EMPTY(src);

		const auto result = std::minmax_element(std::begin(src), std::end(src));
		return std::make_tuple(*result.first, *result.second);
	}

}