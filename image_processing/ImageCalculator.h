#pragma once

#ifndef ImageCalculator_H
#define ImageCalculator_H
#include "IImageCalculator.h"

namespace ip
{
	class ImageCalculator : public IImageCalculator
	{
	public:
		void Abs_I(core::Image_32f& srcDst) const override;

		void Sub_I(core::Image_32f& srcDst, const core::Image_32f& subtrahend) const override;

		void SetWithMask(core::Image_16u& srcDst, const core::Image<bool>& mask, uint16_t value) const override;

		void CopyWithMask(const core::Image_32f& src, const core::Image<bool>& mask, core::Image_32f& dst) const override;
	};
}
#endif