#include "IThresholdingFilterFactory.h"
#include "ThresholdingFilterFactory.h"

namespace ip
{
	IThresholdingFilterFactory::~IThresholdingFilterFactory() = default;

	std::unique_ptr<IThresholdingFilterFactory> CreateThresholdingFilterFactory()
	{
		return std::make_unique<ThresholdingFilterFactory>();
	}
}
