#include "CopyBorderFilter_32f.h"

namespace ip
{
	CopyBorderFilter_32f::CopyBorderFilter_32f(std::shared_ptr<core::IContainerFactory> containerFactory) :
		_containerFactory(std::move(containerFactory))
	{
		ASSERT_NOT_NULL(_containerFactory);
	}

	core::Image_32f CopyBorderFilter_32f::CopyBorder(const core::Image_32f& src, const core::Size& expectedSize)
	{
		ASSERT_IMAGE_NOT_EMPTY(src);
		if (expectedSize.width <= src.width())
			THROW_ARGUMENT_EXCEPTION(expectedSize, "Width of expectedSize is invalid");

		if (expectedSize.height <= src.height())
			THROW_ARGUMENT_EXCEPTION(expectedSize, "Height of expectedSize is invalid");

		auto imageWithBorder = _containerFactory->Create_32f().Image(expectedSize);
		imageWithBorder.fill(0.0f);

		const auto offset_x = (expectedSize.width - src.width()) / 2;
		const auto offset_y = (expectedSize.height - src.height()) / 2;

		auto roi = imageWithBorder.range_s(offset_x, offset_y, src.width(), src.height());
		roi.copy_from(src);

		return imageWithBorder;
	}
}
