#include "ThresholdFilter_LT_32f.h"
#include <algorithm>

void ip::ThresholdFilter_LT_32f::Process(const core::Image_32f& lhr,
	const core::Image_32f& rhr,
	core::Image<bool>& mask) const
{
	ASSERT_IMAGE_NOT_EMPTY(lhr);
	ASSERT_IMAGE_SIZES_EQUAL(lhr, rhr);
	ASSERT_IMAGE_SIZES_EQUAL(rhr, mask);

	std::transform(std::begin(lhr), std::end(lhr), 
		std::begin(rhr), 
		std::begin(mask),
		[](const auto& lhrValue, const auto& rhrValue)
	{
			return lhrValue < rhrValue;
	});
}
