#include "IConverterFactory.h"
#include "ConverterFactory.h"

namespace ip
{
	IConverterFactory::~IConverterFactory() = default;

	std::unique_ptr<IConverterFactory> CreateConverterFactory()
	{
		return std::make_unique<ConverterFactory>();
	}
}
