#include "ThresholdingFilterFactory.h"
#include "ThresholdFilter_LT_32f.h"

namespace ip
{
	std::unique_ptr<IThresholdFilter_32f> ThresholdingFilterFactory::CreateLessThanThresholdFilter_32f() const
	{
		return std::make_unique<ThresholdFilter_LT_32f>();
	}
}
