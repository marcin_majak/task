#pragma once

#ifndef ConverterFactory_H
#define ConverterFactory_H
#include "IConverterFactory.h"

namespace ip
{
	class ConverterFactory : public IConverterFactory
	{
	public:
		std::unique_ptr<IConverter> CreateConverter() override;
	};
}
#endif