#pragma once

#ifndef StatisticsCalculatorFactory_H
#define StatisticsCalculatorFactory_H

#include "IStatisticsCalculatorFactory.h"

namespace ip
{
	class StatisticsCalculatorFactory : public IStatisticsCalculatorFactory
	{
	public:
		std::unique_ptr<IStatisticsCalculator> CreateStatisticsCalculator() const override;
	};
}
#endif