#pragma once

#ifndef IThresholdingFilterFactory_H
#define IThresholdingFilterFactory_H

#include <memory>
#include "IThresholdFilter_32f.h"
#include "ExportDefines.h"

namespace ip
{
	struct IP_API IThresholdingFilterFactory
	{
		virtual ~IThresholdingFilterFactory();

		virtual std::unique_ptr<IThresholdFilter_32f> CreateLessThanThresholdFilter_32f() const = 0;
	};

	std::unique_ptr<IThresholdingFilterFactory> IP_API CreateThresholdingFilterFactory();
}

#endif

