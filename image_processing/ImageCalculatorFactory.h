#pragma once

#ifndef ImageCalculatorFactory_H
#define ImageCalculatorFactory_H
#include "IImageCalculatorFactory.h"

namespace ip
{
	class ImageCalculatorFactory : public IImageCalculatorFactory
	{
	public:
		std::unique_ptr<IImageCalculator> CreateImageCalculator() const override;
	};
}
#endif