#include "ISmoothingFilterFactory.h"
#include "SmoothingFilterFactory.h"

namespace ip
{
	ISmoothingFilterFactory::~ISmoothingFilterFactory() = default;

	std::unique_ptr<ISmoothingFilterFactory> CreateSmoothingFactory(std::shared_ptr<ip::ICopyFilterFactory> copyFilterFactory)
	{
		return std::make_unique<SmoothingFilterFactory>(std::move(copyFilterFactory));
	}
}
