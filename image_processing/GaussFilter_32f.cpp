#include "GaussFilter_32f.h"
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>

namespace ip
{
	GaussFilter_32f::GaussFilter_32f(int gaussWindowSize, std::unique_ptr<ICopyBorderFilter_32f> copyBorderFilter) : 
		_copyBorderFilter(std::move(copyBorderFilter))
	{
		ASSERT_TRUE_OR_EXCEPTION(gaussWindowSize > 0 && gaussWindowSize % 2 == 1, "Invalid gauss window size");
		ASSERT_NOT_NULL(_copyBorderFilter);

		_kernel = core::Image_32f(core::Size(gaussWindowSize, gaussWindowSize), 
			std::make_shared<core::StlImageAllocator<float>>());

		GenerateGauss();
		auto kernelView = _kernel.view();
	}

	void GaussFilter_32f::Process(const core::Image_32f& src, core::Image_32f& dst)
	{
		ASSERT_IMAGE_NOT_EMPTY(src);
		ASSERT_IMAGE_SIZES_EQUAL(src, dst);

		const int padding = int(_kernel.width() / 2) * 2;
		const core::Size sizeWithPadding(src.width() + padding, src.height() + padding);

		const auto imageWithPadding = _copyBorderFilter->CopyBorder(src, sizeWithPadding);
		ApplyFilter(imageWithPadding, dst);
	}

	void GaussFilter_32f::GenerateGauss()
	{
		const float sigma = 0.3f*(_kernel.width() / 2.0f - 1) + 0.8f;

		const auto firstDenominator = 2.0 * std::pow(sigma, 2);
		const auto secondDenominator = M_PI * firstDenominator;

		auto sum = 0.0f;
		for (auto y = 0; y < int(_kernel.height()); y++) 
		{
			for (auto x = 0; x < int(_kernel.width()); x++) 
			{
				const auto yValue = y - (_kernel.height() - 1) / 2.0;
				const auto xValue = x - (_kernel.width() - 1)/ 2.0;

				_kernel[y][x] = static_cast<float>(exp(-(std::pow(yValue, 2) + std::pow(xValue, 2)) / firstDenominator) / secondDenominator);
				sum += _kernel[y][x];
			}
		}

		std::for_each(std::begin(_kernel), std::end(_kernel), [sum](auto& value) { value /= sum; });
	}

	void GaussFilter_32f::ApplyFilter(const core::Image_32f& srcWithPadding, core::Image_32f& dst) const
	{
		dst.fill(0.0f);
		for (auto y = 0; y < int(dst.height()); y++) 
		{
			auto dstPtr = dst.ptr(0, y);
			for (auto x = 0; x < int(dst.width()); x++) 
			{
				for (int h = y; h < y + int(_kernel.height()); h++) 
				{
					for (int w = x; w < x + int(_kernel.width()); w++) 
					{
						const auto srcData = srcWithPadding.ptr(w, h);
						
						const auto yKernel = h - y;
						const auto xKernel = w - x;
						
						*dstPtr += *srcData * _kernel[yKernel][xKernel];
					}
				}

				++dstPtr;
			}
		}
	}
}
