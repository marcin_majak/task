#include "IStatisticsCalculatorFactory.h"
#include "StatisticsCalculatorFactory.h"

namespace ip
{
	IStatisticsCalculatorFactory::~IStatisticsCalculatorFactory() = default;

	std::unique_ptr<IStatisticsCalculatorFactory> CreateStatisticsCalculatorFactory()
	{
		return std::make_unique<StatisticsCalculatorFactory>();
	}
}
