#pragma once

#ifndef StatisticsCalculator_H
#define StatisticsCalculator_H
#include "IStatisticsCalculator.h"

namespace ip
{
	class StatisticsCalculator : public IStatisticsCalculator
	{
	public:
		std::tuple<float, float> GetMinMax(const core::Image_32f& src) const override;
	};
}
#endif