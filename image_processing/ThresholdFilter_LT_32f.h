#pragma once

#ifndef ThresholdFilter_LT_32f_H
#define ThresholdFilter_LT_32f_H
#include "IThresholdFilter_32f.h"

namespace ip
{
	class ThresholdFilter_LT_32f : public IThresholdFilter_32f
	{
	public:
		void Process(const core::Image_32f& lhr, const core::Image_32f& rhr, core::Image<bool>& mask) const override;
	};
}
#endif