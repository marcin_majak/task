#pragma once

#ifndef IConverterFactory_H
#define IConverterFactory_H

#include <memory>
#include "IConverter.h"
#include "ExportDefines.h"

namespace ip
{
	struct IP_API IConverterFactory
	{
		virtual ~IConverterFactory();

		virtual std::unique_ptr<IConverter> CreateConverter() = 0;
	};

	std::unique_ptr<IConverterFactory> IP_API CreateConverterFactory();
}
#endif