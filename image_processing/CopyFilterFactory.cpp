#include "CopyFilterFactory.h"
#include "CopyBorderFilter_32f.h"

namespace ip
{
	CopyFilterFactory::CopyFilterFactory(std::shared_ptr<core::IContainerFactory> containerFactory) :
		_containerFactory(std::move(containerFactory))
	{
		ASSERT_NOT_NULL(_containerFactory);
	}

	std::unique_ptr<::ip::ICopyBorderFilter_32f> CopyFilterFactory::CreateCopyBorderFilter()
	{
		return std::make_unique<CopyBorderFilter_32f>(_containerFactory);
	}
}
