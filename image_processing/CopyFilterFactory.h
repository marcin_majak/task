#pragma once

#ifndef CopyFilterFactory_H
#define CopyFilterFactory_H

#include "ICopyFilterFactory.h"
#include <IContainerFactory.h>

namespace ip
{
	class CopyFilterFactory : public ICopyFilterFactory
	{
	public:
		CopyFilterFactory(std::shared_ptr<core::IContainerFactory> containerFactory);

		std::unique_ptr<ICopyBorderFilter_32f> CreateCopyBorderFilter() override;

	private:
		std::shared_ptr<core::IContainerFactory> _containerFactory;
	};
}

#endif