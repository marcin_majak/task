#pragma once

#ifndef GaussFilter_32f_H
#define GaussFilter_32f_H

#include <memory>
#include "IFilter_32f.h"
#include "ICopyBorderFilter_32f.h"

namespace ip
{
	class GaussFilter_32f : public IFilter_32f
	{
	public:
		GaussFilter_32f(int gaussWindowSize, 
			std::unique_ptr<ICopyBorderFilter_32f> copyBorderFilter);

		void Process(const core::Image_32f& src, core::Image_32f& dst) override;

	private:
		void GenerateGauss();
		void ApplyFilter(const core::Image_32f& srcWithPadding, core::Image_32f& dst) const;

		core::Image_32f _kernel;
		std::unique_ptr<ICopyBorderFilter_32f> _copyBorderFilter;
	};
}
#endif