#pragma once

#ifndef IConverter_H
#define IConverter_H
#include <Image.h>
#include "ExportDefines.h"

namespace ip
{
	struct IP_API IConverter
	{
		virtual ~IConverter();

		virtual void Convert(const core::Image_8u_C3& src, core::Image_32f& dst) const = 0;

		virtual void Convert(const core::Image_32f& src, float srcMinValue, float srcMaxValue, core::Image_8u& dst) const = 0;
	};
}
#endif