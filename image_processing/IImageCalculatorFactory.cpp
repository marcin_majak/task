#include "IImageCalculatorFactory.h"
#include "ImageCalculatorFactory.h"

namespace ip
{
	IImageCalculatorFactory::~IImageCalculatorFactory() = default;

	std::unique_ptr<IImageCalculatorFactory> CreateImageCalculatorFactory()
	{
		return std::make_unique<ImageCalculatorFactory>();
	}
}
