#pragma once

#ifndef IFilter_32f_H
#define IFilter_32f_H
#include <Image.h>
#include "ExportDefines.h"

namespace ip
{
	struct IP_API IFilter_32f
	{
		virtual ~IFilter_32f();

		virtual void Process(const core::Image_32f& src, core::Image_32f& dst) = 0;
	};
}
#endif