#pragma once

#ifndef IImageCalculatorFactory_H
#define IImageCalculatorFactory_H

#include <memory>
#include "IImageCalculator.h"
#include "ExportDefines.h"

namespace ip
{
	struct IP_API IImageCalculatorFactory
	{
		virtual ~IImageCalculatorFactory();

		virtual std::unique_ptr<IImageCalculator> CreateImageCalculator() const = 0;
	};

	std::unique_ptr<IImageCalculatorFactory> IP_API CreateImageCalculatorFactory();
}

#endif