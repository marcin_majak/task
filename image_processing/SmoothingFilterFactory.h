#pragma once

#ifndef SmoothingFilterFactory_H
#define SmoothingFilterFactory_H

#include "ISmoothingFilterFactory.h"
#include "ICopyFilterFactory.h"

namespace ip
{
	class SmoothingFilterFactory : public ISmoothingFilterFactory
	{
	public:
		SmoothingFilterFactory(std::shared_ptr<ip::ICopyFilterFactory> copyFilterFactory);

		std::unique_ptr<IFilter_32f> CreateGaussFilter(int windowSize) override;

	private:
		std::shared_ptr<ip::ICopyFilterFactory> _copyFilterFactory;
	};
}

#endif