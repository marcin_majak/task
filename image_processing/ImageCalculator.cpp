#include "ImageCalculator.h"
#include <algorithm>

namespace ip
{
	void ImageCalculator::Abs_I(core::Image_32f& srcDst) const
	{
		ASSERT_IMAGE_NOT_EMPTY(srcDst);

		std::for_each(std::begin(srcDst), std::end(srcDst), [](auto& value)
			{value = std::abs(value); });
	}

	void ImageCalculator::Sub_I(core::Image_32f& srcDst, const core::Image_32f& subtrahend) const
	{
		ASSERT_IMAGE_NOT_EMPTY(srcDst);
		ASSERT_IMAGE_SIZES_EQUAL(srcDst, subtrahend);

		std::transform(std::begin(subtrahend), std::end(subtrahend), 
			std::begin(srcDst), 
			std::begin(srcDst),
			[](const auto& a, const auto& b) {return b - a; });
	}

	void ImageCalculator::SetWithMask(core::Image_16u& srcDst, const core::Image<bool>& mask, uint16_t value) const
	{
		ASSERT_IMAGE_NOT_EMPTY(srcDst);
		ASSERT_IMAGE_SIZES_EQUAL(srcDst, mask);

		std::transform(std::begin(mask), std::end(mask),
			std::begin(srcDst),
			std::begin(srcDst),
			[value](const auto& maskValue, const auto& currentValue) { return maskValue ? value : currentValue; });
	}

	void ImageCalculator::CopyWithMask(const core::Image_32f& src, 
		const core::Image<bool>& mask,
		core::Image_32f& dst) const
	{
		ASSERT_IMAGE_NOT_EMPTY(src);
		ASSERT_IMAGE_SIZES_EQUAL(src, mask);
		ASSERT_IMAGE_SIZES_EQUAL(dst, mask);

		auto srcIter = std::begin(src);
		auto maskIter = std::begin(mask);
		auto dstIter = std::begin(dst);

		for (;srcIter != std::end(src); ++srcIter, ++maskIter, ++dstIter)
		{
			if (*maskIter)
			{
				*dstIter = *srcIter;
			}
		}
	}
}
